import React from 'react';
import { render, waitFor } from '@testing-library/react';
import { SentryDataSource } from './../datasource';
import { SentryVariableEditor } from './SentryVariableEditor';
import { SentryVariableQuery } from './../types';

jest.mock('@grafana/runtime', () => ({
  ...((jest.requireActual('@grafana/runtime') as unknown) as object),
  getTemplateSrv: () => ({
    replace: (s: string) => s,
    getVariables: () => [],
  }),
}));

describe('SentryVariableEditor', () => {
  it('render error when orgId is not available', () => {
    const datasource = {} as SentryDataSource;
    datasource.getOrgSlug = jest.fn(() => '');
    const query = {} as SentryVariableQuery;
    const onChange = jest.fn();
    const result = render(<SentryVariableEditor datasource={datasource} query={query} onChange={onChange} />);
    expect(result.container.firstChild).not.toBeNull();
    expect(result.getByTestId('error-message')).toBeInTheDocument();
  });
  it('render without error', () => {
    const datasource = {} as SentryDataSource;
    datasource.getOrgSlug = jest.fn(() => 'foo');
    const query = {} as SentryVariableQuery;
    const onChange = jest.fn();
    const result = render(<SentryVariableEditor datasource={datasource} query={query} onChange={onChange} />);
    expect(result.container.firstChild).not.toBeNull();
    expect(result.queryByTestId('error-message')).not.toBeInTheDocument();
  });
  describe('projects', () => {
    it(`should render projects filter for projects query`, async () => {
      const datasource = {} as SentryDataSource;
      datasource.getOrgSlug = jest.fn(() => 'foo');
      datasource.getOrganizations = jest.fn(() => Promise.resolve([]));
      const query = { type: 'projects' } as SentryVariableQuery;
      const onChange = jest.fn();
      const result = render(<SentryVariableEditor datasource={datasource} query={query} onChange={onChange} />);
      await waitFor(() => {
        expect(result.container.firstChild).not.toBeNull();
        expect(result.queryByTestId('error-message')).not.toBeInTheDocument();
        expect(result.queryByTestId('variable-query-editor-environments-filter')).not.toBeInTheDocument();
      });
    });
  });
  describe('environments', () => {
    it(`should render environments filters for environments query`, async () => {
      const datasource = {} as SentryDataSource;
      datasource.getOrgSlug = jest.fn(() => 'foo');
      datasource.getProjects = jest.fn(() => Promise.resolve([]));
      const query = { type: 'environments' } as SentryVariableQuery;
      const onChange = jest.fn();
      const result = render(<SentryVariableEditor datasource={datasource} query={query} onChange={onChange} />);
      await waitFor(() => {
        expect(result.container.firstChild).not.toBeNull();
        expect(result.queryByTestId('error-message')).not.toBeInTheDocument();
        expect(result.getByTestId('variable-query-editor-environments-filter')).toBeInTheDocument();
      });
    });
  });
});
