import React from 'react';
import { render } from '@testing-library/react';
import { DataSourceInstanceSettings } from '@grafana/data';
import { ScopePicker } from './ScopePicker';
import { SentryDataSource } from './../../datasource';
import { SentryQuery, SentryConfig } from './../../types';

jest.mock('@grafana/runtime', () => ({
  ...((jest.requireActual('@grafana/runtime') as unknown) as object),
  getTemplateSrv: () => ({
    replace: (s: string) => s,
    getVariables: () => [],
  }),
}));

describe('ScopePicker', () => {
  it('should render without error', () => {
    const datasource = new SentryDataSource({} as DataSourceInstanceSettings<SentryConfig>);
    const query = {} as SentryQuery;
    const onChange = jest.fn();
    const onRunQuery = jest.fn();
    const result = render(
      <ScopePicker datasource={datasource} query={query} onChange={onChange} onRunQuery={onRunQuery} />
    );
    expect(result.container.firstChild).not.toBeNull();
  });
});
