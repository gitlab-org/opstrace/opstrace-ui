import { replaceProjectIDs } from './replace';

let mockReplace = jest.fn();
jest.mock('@grafana/runtime', () => ({
  ...((jest.requireActual('@grafana/runtime') as unknown) as object),
  getTemplateSrv: () => ({
    replace: mockReplace,
    getVariables: () => [],
  }),
}));

describe('replace', () => {
  beforeEach(() => {
    mockReplace = jest.fn();
  });
  describe('replaceProjectIDs', () => {
    it('default replaceProjectIDs should return valid objects', () => {
      mockReplace.mockImplementation((s) => s);
      const a = replaceProjectIDs(['hello', 'world']);
      expect(a).toStrictEqual(['hello', 'world']);
    });
    it('list with variables passed to replaceProjectIDs should return valid objects', () => {
      mockReplace.mockImplementation((s) => (s === '${attr}' ? 'foo' : s));
      const a = replaceProjectIDs(['hello', '${attr}', 'world']);
      expect(a).toStrictEqual(['hello', 'foo', 'world']);
    });
    it('var with multiple value replaceProjectIDs should return valid objects', () => {
      mockReplace.mockImplementation((s) => (s === '${attr}' ? 'foo,bar' : s));
      const a = replaceProjectIDs(['hello', '${attr}', 'world']);
      expect(a).toStrictEqual(['hello', 'foo', 'bar', 'world']);
    });
  });
});
