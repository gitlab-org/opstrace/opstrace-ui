package main

import (
	"os"

	"gitlab-errortracking-datasource/pkg/plugin"

	"github.com/grafana/grafana-plugin-sdk-go/backend"
	"github.com/grafana/grafana-plugin-sdk-go/backend/datasource"
)

func main() {
	backend.SetupPluginEnvironment(plugin.PluginID)
	err := datasource.Serve(plugin.NewDatasource())
	if err != nil {
		backend.Logger.Error("error loading plugin", "pluginId", plugin.PluginID)
		os.Exit(1)
	}
}
