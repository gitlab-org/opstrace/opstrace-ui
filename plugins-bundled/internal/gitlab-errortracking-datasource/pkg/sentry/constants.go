package sentry

const (
	PluginID         string = "gitlab-errortracking-datasource"
	DefaultSentryURL string = "https://sentry.io"
)
