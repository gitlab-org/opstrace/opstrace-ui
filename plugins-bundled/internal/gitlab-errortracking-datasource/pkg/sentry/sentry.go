package sentry

import (
	"bytes"
	"compress/gzip"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strings"

	"github.com/grafana/grafana-plugin-sdk-go/backend"
	"github.com/grafana/grafana-plugin-sdk-go/build"
)

type apiProvider interface {
	// Fetch is general purpose api
	Fetch(path string, out interface{}) error

	// GetOrganizations list the organizations
	// https://docs.sentry.io/api/organizations/list-your-organizations/
	GetOrganizations() ([]SentryOrganization, error)

	// GetProjects List an Organization's Projects
	// https://docs.sentry.io/api/organizations/list-an-organizations-projects/
	GetProjects(organizationSlug string) ([]SentryProject, error)

	// GetIssues list the issues for an organization
	// Organization Slug is the mandatory parameter
	// From and To times will be grafana dashboard's range
	// https://github.com/getsentry/sentry/blob/master/src/sentry/api/endpoints/organization_group_index.py#L158
	GetIssues(gii GetIssuesInput) ([]SentryIssue, string, error)

	// GetStatsV2 list the stats for an organization
	GetStatsV2(args GetStatsV2Input) (StatsV2Response, string, error)
}

type SentryClient struct {
	BaseURL          string
	OrgSlug          string
	authToken        string
	sentryHttpClient *HTTPClient
	apiProvider
}

func NewSentryClient(
	baseURL string,
	orgSlug string,
	authToken string,
	doerClient doer,
	gobCookiePresent bool,
) (*SentryClient, error) {
	client := &SentryClient{
		BaseURL:   DefaultSentryURL,
		OrgSlug:   orgSlug,
		authToken: authToken,
	}
	if baseURL != "" {
		client.BaseURL = baseURL
	}
	client.sentryHttpClient = NewHTTPClient(
		doerClient,
		PluginID,
		build.GetBuildInfo,
		client.authToken,
		gobCookiePresent,
	)
	return client, nil
}

type SentryErrorResponse struct {
	Detail string `json:"detail"`
}

func (sc *SentryClient) AttachHeaders(headers map[string]string) {
	sc.sentryHttpClient.AttachHeaders(headers)
}

func isGzipEncoded(req *http.Response) bool {
	return req.Header.Get("Content-Encoding") == "gzip"
}

func readCompressedBody(res *http.Response) ([]byte, error) {
	r, err := gzip.NewReader(res.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to init gzip reader from res body: %w", err)
	}
	defer func(r *gzip.Reader) {
		err := r.Close()
		if err != nil {
			backend.Logger.Error("failed to close gzip reader: ", err)
		}
		err = res.Body.Close()
		if err != nil {
			backend.Logger.Error("failed to close res body: ", err)
		}
	}(r)
	bts, err := io.ReadAll(r)
	if err != nil {
		return nil, fmt.Errorf("failed to read gzip content: %w", err)
	}
	return bts, nil
}

func readBody(res *http.Response) ([]byte, error) {
	// Parse the http request body into a string and unmarshall it manually.
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			backend.Logger.Error("failed to close resp body: ", err)
		}
	}(res.Body)
	bts, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read request body: %w", err)
	}
	return bts, nil
}

func (sc *SentryClient) Fetch(path string, out interface{}) error {
	req, _ := http.NewRequest(http.MethodGet, sc.BaseURL+path, nil)
	res, err := sc.sentryHttpClient.Do(req)
	if err != nil {
		return err
	}
	var (
		payload []byte
		buffer  *bytes.Buffer
	)
	// if response is gzip encoded
	if isGzipEncoded(res) {
		payload, err = readCompressedBody(res)
	} else {
		payload, err = readBody(res)
	}
	if err != nil {
		return fmt.Errorf("failed to read body from response: %w", err)
	}
	buffer = bytes.NewBuffer(payload)
	if res.StatusCode == http.StatusOK {
		if err := json.NewDecoder(buffer).Decode(&out); err != nil {
			return err
		}
	} else {
		var errResponse SentryErrorResponse
		if err := json.NewDecoder(buffer).Decode(&errResponse); err != nil {
			errorMessage := strings.TrimSpace(fmt.Sprintf("%s %s", res.Status, err.Error()))
			return errors.New(errorMessage)
		}
		errorMessage := strings.TrimSpace(fmt.Sprintf("%s %s", res.Status, errResponse.Detail))
		return errors.New(errorMessage)
	}
	return err
}
