# Developer guide

This guide helps you get started developing GitLab Observability UI.

Since GitLab Observability UI is based on Grafana, before you begin, you might want to read [How to contribute to Grafana as a junior dev](https://medium.com/@ivanahuckova/how-to-contribute-to-grafana-as-junior-dev-c01fe3064502) by [Ivana Huckova](https://medium.com/@ivanahuckova).

You can also [read the glossary](glossary.md) to familiarize yourself with the terminology.

## Dependencies

Make sure you have the following dependencies installed before setting up your developer environment:

- [Git](https://git-scm.com/)
- [Go](https://golang.org/dl/) (see [go.mod](../go.mod#L3) for minimum required version)
- [Mage](https://magefile.org)
- [Node.js (Long Term Support)](https://nodejs.org)
- [Yarn](https://yarnpkg.com)
- [Watchman](https://facebook.github.io/watchman) ( needed to fix a "Too many files open" error when running JS tests )


### asdf

You can use [asdf](https://asdf-vm.com) to install versioned dependencies on macOS and Linux.

Run `asdf install` to install the dependencies listed in the `.tool-versions` file.

This will prompt you to add any plugins you may require.

`asdf` doesn't have a plugin for watchman, but it's [easily installed](https://facebook.github.io/watchman/docs/install.html).

### macOS

Alternatively use [Homebrew](https://brew.sh/) to install any missing dependencies:

```
brew install git
brew install go
brew install node@14
brew install watchman
brew install mage

npm install -g yarn
```


## Build GitLab Observability UI

GitLab Observability UI consists of two components: the _frontend_, and the _backend_.

### Frontend

Before we can build the frontend assets, we need to install the dependencies:

```
yarn install --pure-lockfile
```

After the command has finished, we can start building our source code:

```
yarn start
```

Once `yarn start` has built the assets, it will continue to do so whenever any of the files change. This means you don't have to manually build the assets every time you change the code.

Next, we'll build the web server that will serve the frontend assets we just built.

### Backend

Build and run the backend by running `make run` in the root directory of the repository. This command compiles the Go source code and starts a web server.

> Are you having problems with [too many open files](#troubleshooting)?

By default, you can access the web server at `http://localhost:3001/`.

Log in using the default credentials:

| username | password |
| -------- | -------- |
| `admin`  | `admin`  |

When you log in for the first time, GitLab Observability UI asks you to change your password.

### Bundled Plugins

See [README](/plugins-bundled/README.md)

## Test GitLab Observability UI

The test suite consists of three types of tests: _Frontend tests_, _backend tests_, and _end-to-end tests_.

### Run frontend tests

We use [jest](https://jestjs.io/) for our frontend tests. Run them using Yarn:

```
make test-js

# or

yarn test
```

### Run backend tests

If you're developing for the backend, run the tests with the standard Go tool:

```
make test-go

#or

go test -v ./pkg/...
```

### Run all backend and frontend unit tests

```
make test
```

### Run end-to-end tests

The end to end tests GitLab Observability UI use [Cypress](https://www.cypress.io/) to run automated scripts in a headless Chromium browser. Read more about our [e2e framework](/contribute/style-guides/e2e.md).

To run the tests:

```
yarn e2e
```

By default, the end-to-end tests starts a GitLab Observability UI instance listening on `localhost:3001`. To use a specific URL, set the `BASE_URL` environment variable:

```
BASE_URL=http://localhost:3333 yarn e2e
```

To follow the tests in the browser while they're running, use the `yarn e2e:debug`.

```
yarn e2e:debug
```

If you want to pick a test first, use the `yarn e2e:dev`, to pick a test and follow the test in the browser while it runs.

```
yarn e2e:dev
```

## Configure GitLab Observability UI

The default configuration is located in `conf/defaults.ini`. You should almost never make changes to this file directly, but rather override the options you need in the custom configuration files.

The default configation can be overriden by specifing the `--config` parameter.

The custom configuration file used for development is located in `conf/dev.ini`. Here you can make changes to the configuration and they will be applied when running the dev server with `make run`.

The custom configuration overrides file used by default for production is located in `conf/custom.ini`. **Note** this is not currently used by GOB. (see below)

### GOB Configuration

GOB defines its own configuration overrides when deploying GOUI. It does so by specifying a custom `grafana.ini` (by setting `--config` parameter) and by setting some Env variables. GOB does not rely on `conf/custom.ini`.

- `grafana.ini`: https://gitlab.com/gitlab-org/opstrace/opstrace/-/blob/b8a7a1f5190127bef150630b9ebd882d894eea52/tenant-operator/controllers/tenant/argus/config.go
- Env vars: https://gitlab.com/gitlab-org/opstrace/opstrace/-/blob/b8a7a1f5190127bef150630b9ebd882d894eea52/tenant-operator/controllers/tenant/argus/statefulset.go

#### GitLab Embedding configuration

In order for GOUI/GOB to work correctly when embedded within GitLab, [embedding_parent_url](/conf/defaults.ini#333) should be set to the appropriate GitLab URL e.g.

```
embedding_parent_url= https://www.gitlab.com # https://www.staging.gitlab.com for staging etc
```

## Add data sources

By now, you should be able to build and test a change you've made to the GitLab Observability UI source code. In most cases, you need to add at least one data source to verify the change.

To set up data sources for your development environment, go to the [devenv](/devenv) directory in the GitLab Observability UI repository:

```
cd devenv
```

Run the `setup.sh` script to set up a set of data sources and dashboards in your local GitLab Observability UI instance. The script creates a set of data sources called **gdev-\<type\>**, and a set of dashboards located in a folder called **gdev dashboards**.

Some of the data sources require databases to run in the background.

Installing and configuring databases can be a tricky business. GitLab Observability UI uses [Docker](https://docker.com) to make the task of setting up databases a little easier. Make sure you [install Docker](https://docs.docker.com/docker-for-mac/install/) before proceeding to the next step.

In the root directory of your GrafanGitLab Observability UIa repository, run the following command:

```
make devenv sources=influxdb,loki
```

The script generates a Docker Compose file with the databases you specify as `sources`, and runs them in the background.

See the repository for all the [available data sources](/devenv/docker/blocks). Note that some data sources have specific Docker images for macOS, e.g. `nginx_proxy_mac`.

## Run a Docker image

You can download images built from CI published under GitLab Observability UI's [Container Registry](https://gitlab.com/gitlab-org/opstrace/opstrace-ui/container_registry) and run it locally

To run the latest stable build:

```
docker run -p 3001:3001 registry.gitlab.com/gitlab-org/opstrace/opstrace-ui/gitlab-observability-ui:latest
```

To run a build associated with a specific commit in `main`:

```
docker run -p 3001:3001 registry.gitlab.com/gitlab-org/opstrace/opstrace-ui/gitlab-observability-ui:SHA_COMMIT
```

<!-- ## Build a Docker image

TODO: Clean up Docker image building task and docs https://gitlab.com/gitlab-org/opstrace/opstrace-ui/-/issues/63

To build a Docker image, run:

```
make build-docker-full
```

The resulting image will be tagged as grafana/grafana:dev.

> **Note:** If you've already set up a local development environment, and you're running a `linux/amd64` machine, you can speed up building the Docker image:

1. Build the frontend: `go run build.go build-frontend`.
1. Build the Docker image: `make build-docker-dev`.

**Note:** If you are using Docker for macOS, be sure to set the memory limit to be larger than 2 GiB. Otherwise, `grunt build` may fail. The memory limit settings are available under **Docker Desktop** -> **Preferences** -> **Advanced**. -->

## Run GitLab Observability UI with GDK

### Local GDK

- Create `env.runit` file if it does not exist already in the root GDK folder
- Add `export OVERRIDE_OBSERVABILITY_URL=http://localhost:3001` ( or whatever instance of GitLab Observability UI you want to test)
- Add `export STANDALONE_OBSERVABILITY_UI=true` if you are running Observability UI locally (without Observability backend)
- `gdk restart`
- Enable the Observability Feature

```
rails c

Feature.enable(:observability_group_tab)
```

**Note** that if you want to test local GDK with staging/prod GOUI (i.e. `observe.staging.gitlab.com` or `observe.gitlab.com`) you need to authenticate GOUI on a separate browser tab as authentication will fail when GOUI embedded in a local GDK instance (that's because `observe.staging.gitlab.com` or `observe.gitlab.com` will try to authenticate against `staging.gitlab.com` or `gitlab.com`, and it will not load inside an an iframe).

### Local GitLab Observability UI

If you want to run the UI locally, you just need to follow the step above. In short,

```
# Run the backend
make run

# Run the frontend
yarn start
```

If you are running GDK on a custom URL/port, you need to set the `GF_SECURITY_EMBEDDING_PARENT_URL` config when running the backend. This is needed so that communication between GOUI and GDK works correctly. By default, when running locally, it expects GDK to be running under `http://127.0.0.1:3000`

```
GF_SECURITY_EMBEDDING_PARENT_URL="https://local.gitlab.com:3443" make run
```

If you want to run the UI from a Docker dev image, you need to specify the following Env variables

```
GF_SECURITY_ALLOW_EMBEDDING=true
GF_SECURITY_COOKIE_SAMESITE='none'
GF_SECURITY_COOKIE_SECURE='true'
GF_SECURITY_CONTENT_SECURITY_POLICY=false
GF_SECURITY_EMBEDDING_PARENT_URL='http://127.0.0.1:3000' # insert GDK URL here
```

e.g.

```
docker run -p 3001:3001 -e GF_SECURITY_ALLOW_EMBEDDING=true -e GF_SECURITY_COOKIE_SAMESITE='none' -e GF_SECURITY_COOKIE_SECURE='true' -e GF_SECURITY_CONTENT_SECURITY_POLICY=false registry.gitlab.com/gitlab-org/opstrace/opstrace-ui/gitlab-observability-ui:latest
```

## Troubleshooting

Are you having issues with setting up your environment? Here are some tips that might help.

### Too many open files when running `make run`

Depending on your environment, you may have to increase the maximum number of open files allowed. For the rest of this section, we will assume you are on a Unix like OS (e.g. Linux/macOS), where you can control the maximum number of open files through the [ulimit](https://ss64.com/bash/ulimit.html) shell command.

To see how many open files are allowed, run:

```
ulimit -a
```

To change the number of open files allowed, run:

```
ulimit -S -n 2048
```

The number of files needed may be different on your environment. To determine the number of open files needed by `make run`, run:

```
find ./conf ./pkg ./public/views | wc -l
```

Another alternative is to limit the files being watched.

To retain your `ulimit` configuration, i.e. so it will be remembered for future sessions, you need to commit it to your command line shell initialization file. Which file this will be depends on the shell you are using, here are some examples:

- zsh -> ~/.zshrc
- bash -> ~/.bashrc

Commit your ulimit configuration to your shell initialization file as follows ($LIMIT being your chosen limit and $INIT_FILE being the initialization file for your shell):

```
echo ulimit -S -n $LIMIT >> $INIT_FILE
```

Your command shell should read the initialization file in question every time it gets started, and apply your `ulimit` command.

For some people, typically using the bash shell, ulimit fails with an error similar to the following:

```
ulimit: open files: cannot modify limit: Operation not permitted
```

If that happens to you, chances are you've already set a lower limit and your shell won't let you set a higher one. Try looking in your shell initialization files (~/.bashrc typically), if there's already a ulimit command that you can tweak.

### Cannot install yarn dependencies

When trying to install new Yarn dependencies, you might occasionally face installation errors such as:

```
yarn add <some-package>
....
expected workspace package to exist for \"rimraf\"
```

This seems to be caused by a bug in Yarn 1 (https://github.com/yarnpkg/yarn/issues/7734), which has been fixed with Yarn 2. 
This bug only affects Yarn > 1.19.0, so one quick workaround is to downgrade before installing a new dependency. e.g.

```
yarn policies set-version 1.19.0 # downgrade
yarn install <your-package>
yarn policies set-version 1.22.19 # back to latest v1 
```

## Next steps

- Read our [style guides](style-guides).
- Read about the [architecture](architecture).
- Read [How to contribute to Grafana as a junior dev](https://medium.com/@ivanahuckova/how-to-contribute-to-grafana-as-junior-dev-c01fe3064502) by [Ivana Huckova](https://medium.com/@ivanahuckova).
