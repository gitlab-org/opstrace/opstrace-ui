# Development environment

## Local

Please follow [this issue](https://gitlab.com/gitlab-org/opstrace/opstrace-ui/-/issues/12).

## Gitpod

Please follow [this issue](https://gitlab.com/gitlab-org/opstrace/opstrace-ui/-/issues/13).
