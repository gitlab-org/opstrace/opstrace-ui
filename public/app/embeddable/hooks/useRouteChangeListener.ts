import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { StoreState } from 'app/types';
import { dispatchMessage } from '../utils/dispatchMessage';
import { MESSAGE_EVENT_TYPE } from '../types';

export function useRouteChangeListener() {
  const url = useSelector((state: StoreState) => state?.location?.url);
  useEffect(() => {
    dispatchMessage({ type: MESSAGE_EVENT_TYPE.GOUI_ROUTE_UPDATE, payload: { url } });
  }, [url]);
}
