import { useEffect } from 'react';
import { dispatchMessage } from '../utils/dispatchMessage';
import { MESSAGE_EVENT_TYPE } from '../types';

export function useLoadEventListener() {
  useEffect(() => {
    const handleLoad = () => {
      dispatchMessage({ type: MESSAGE_EVENT_TYPE.GOUI_LOADED });
    };

    if (document.readyState === 'loading') {
      // Loading hasn't finished yet
      document.addEventListener('DOMContentLoaded', handleLoad);
    } else {
      // `DOMContentLoaded` has already fired
      handleLoad();
    }

    return () => {
      window.removeEventListener('DOMContentLoaded', handleLoad);
    };
  }, []);
}
