import React from 'react';
import { shallow } from 'enzyme';
import DashboardsTable, { Props } from './DashboardsTable';
import { PluginDashboard } from '../../types';

const setup = (propOverrides?: object) => {
  const props: Props = {
    dashboards: [] as PluginDashboard[],
    onImport: jest.fn(),
    onRemove: jest.fn(),
  };

  Object.assign(props, propOverrides);

  return shallow(<DashboardsTable {...props} />);
};

describe('Render', () => {
  it('should render component', () => {
    const wrapper = setup();

    expect(wrapper).toMatchSnapshot();
  });
});
