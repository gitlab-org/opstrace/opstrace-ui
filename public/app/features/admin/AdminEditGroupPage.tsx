import React, { FC, useState, useEffect } from 'react';
import Page from 'app/core/components/Page/Page';
import { useSelector } from 'react-redux';
import { StoreState, GroupUser } from 'app/types';
import { getNavModel } from 'app/core/selectors/navModel';
import UsersTable from '../users/UsersTable';
import { useAsyncFn } from 'react-use';
import { getBackendSrv } from '@grafana/runtime';
import { UrlQueryValue } from '@grafana/data';
import { Form, Field, Input, Button, Legend } from '@grafana/ui';
import { css } from 'emotion';

interface GroupNameDTO {
  groupName: string;
}

const getGroup = async (orgId: UrlQueryValue) => {
  return await getBackendSrv().get('/api/groups/' + orgId);
};

const getGroupUsers = async (orgId: UrlQueryValue) => {
  return await getBackendSrv().get('/api/groups/' + orgId + '/users');
};

const updateGroupUserRole = async (orgUser: GroupUser, orgId: UrlQueryValue) => {
  await getBackendSrv().patch('/api/groups/' + orgId + '/users/' + orgUser.userId, orgUser);
};

const removeGroupUser = async (orgUser: GroupUser, orgId: UrlQueryValue) => {
  return await getBackendSrv().delete('/api/groups/' + orgId + '/users/' + orgUser.userId);
};

export const AdminEditGroupPage: FC = () => {
  const navIndex = useSelector((state: StoreState) => state.navIndex);
  const navModel = getNavModel(navIndex, 'global-groups');

  const orgId = useSelector((state: StoreState) => state.location.routeParams.id);

  const [users, setUsers] = useState<GroupUser[]>([]);

  const [groupState, fetchGroup] = useAsyncFn(() => getGroup(orgId), []);
  const [, fetchGroupUsers] = useAsyncFn(() => getGroupUsers(orgId), []);

  useEffect(() => {
    fetchGroup();
    fetchGroupUsers().then((res) => setUsers(res));
  }, []);

  const updateGroupName = async (name: string) => {
    return await getBackendSrv().put('/api/groups/' + orgId, { ...groupState.value, name });
  };

  return (
    <Page navModel={navModel}>
      <Page.Contents>
        <>
          <Legend>Edit Group</Legend>

          {groupState.value && (
            <Form
              defaultValues={{ groupName: groupState.value.name }}
              onSubmit={async (values: GroupNameDTO) => await updateGroupName(values.groupName)}
            >
              {({ register, errors }) => (
                <>
                  <Field label="Name" invalid={!!errors.groupName} error="Name is required">
                    <Input name="groupName" ref={register({ required: true })} />
                  </Field>
                  <Button>Update</Button>
                </>
              )}
            </Form>
          )}

          <div
            className={css`
              margin-top: 20px;
            `}
          >
            <Legend>Group Users</Legend>
            {!!users.length && (
              <UsersTable
                users={users}
                onRoleChange={(role, orgUser) => {
                  updateGroupUserRole({ ...orgUser, role }, orgId);
                  setUsers(
                    users.map((user) => {
                      if (orgUser.userId === user.userId) {
                        return { ...orgUser, role };
                      }
                      return user;
                    })
                  );
                  fetchGroupUsers();
                }}
                onRemoveUser={(orgUser) => {
                  removeGroupUser(orgUser, orgId);
                  setUsers(users.filter((user) => orgUser.userId !== user.userId));
                  fetchGroupUsers();
                }}
              />
            )}
          </div>
        </>
      </Page.Contents>
    </Page>
  );
};

export default AdminEditGroupPage;
