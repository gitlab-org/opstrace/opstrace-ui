import React, { FC, useEffect } from 'react';
import { getNavModel } from 'app/core/selectors/navModel';
import Page from 'app/core/components/Page/Page';
import { useSelector } from 'react-redux';
import { StoreState } from 'app/types/store';
import { LinkButton } from '@grafana/ui';
import { getBackendSrv } from '@grafana/runtime';
import { AdminGroupsTable } from './AdminGroupsTable';
import useAsyncFn from 'react-use/lib/useAsyncFn';

const deleteGroup = async (orgId: number) => {
  return await getBackendSrv().delete('/api/groups/' + orgId);
};

const getGroups = async () => {
  return await getBackendSrv().get('/api/groups');
};

export const AdminListGroupsPage: FC = () => {
  const navIndex = useSelector((state: StoreState) => state.navIndex);
  const navModel = getNavModel(navIndex, 'global-groups');
  const [state, fetchGroups] = useAsyncFn(async () => await getGroups(), []);

  useEffect(() => {
    fetchGroups();
  }, []);

  return (
    <Page navModel={navModel}>
      <Page.Contents>
        <>
          <div className="page-action-bar">
            <div className="page-action-bar__spacer" />
            <LinkButton icon="plus" href="group/new">
              New group
            </LinkButton>
          </div>
          {state.loading && 'Fetching groups'}
          {state.error}
          {state.value && (
            <AdminGroupsTable
              groups={state.value}
              onDelete={(orgId) => {
                deleteGroup(orgId).then(() => fetchGroups());
              }}
            />
          )}
        </>
      </Page.Contents>
    </Page>
  );
};

export default AdminListGroupsPage;
