import React, { FC } from 'react';
import { GroupRole } from '@grafana/data';
import { Select } from '@grafana/ui';

interface Props {
  value: GroupRole;
  onChange: (role: GroupRole) => void;
}

const options = Object.keys(GroupRole).map((key) => ({ label: key, value: key }));

export const GroupRolePicker: FC<Props> = ({ value, onChange, ...restProps }) => (
  <Select
    value={value}
    options={options}
    onChange={(val) => onChange(val.value as GroupRole)}
    placeholder="Choose role..."
    {...restProps}
  />
);
