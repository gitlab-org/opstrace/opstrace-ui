import { fireEvent, render, screen } from '@testing-library/react';
import React from 'react';
import { Provider } from 'react-redux';

import { configureStore } from '../../../../store/configureStore';
import { DashboardModel } from '../../state';

import DashNav from './DashNav';

import { asMock } from 'app/core/utils/testUtils';
import { PlaylistSrv } from 'app/features/playlist/playlist_srv';
import { getLocationSrv } from '@grafana/runtime';
import { isEmbedded } from 'app/embeddable/utils/isEmbedded';

jest.mock('app/embeddable/utils/isEmbedded');

jest.mock('app/features/dashboard/services/TimeSrv', () => ({
  getTimeSrv: jest.fn().mockReturnValue({
    getValidIntervals(): string[] {
      return ['1d'];
    },
    timeRange() {
      return { from: 'now', to: 'now-1h', raw: { from: 'now', to: 'now-1h' } };
    },
  }),
}));

jest.mock('@grafana/runtime');

const SEARCH_BUTTON_LABEL = 'Page toolbar button Search';

const renderComponent = () => {
  let dashboard = new DashboardModel({});
  const store = configureStore();
  const injectorMock = {
    get: (name: string) => {
      switch (name) {
        case 'playlistSrv':
          return PlaylistSrv;
        default:
          throw { message: 'Unknown service ' + name };
      }
    },
  };

  render(
    <Provider store={store}>
      <DashNav dashboard={dashboard} isFullscreen={false} onAddPanel={() => {}} $injector={injectorMock} />
    </Provider>
  );
};
describe('DashNav', () => {
  it('will render search button when in iframe', () => {
    asMock(isEmbedded).mockReturnValueOnce(true);
    renderComponent();
    expect(screen.queryByLabelText(SEARCH_BUTTON_LABEL)).toBeInTheDocument();
  });

  it('will not render search button when not in iframe', () => {
    asMock(isEmbedded).mockReturnValueOnce(false);
    renderComponent();

    expect(screen.queryByLabelText(SEARCH_BUTTON_LABEL)).not.toBeInTheDocument();
  });

  it('when clicking on search button, should update query param with search', () => {
    asMock(isEmbedded).mockReturnValueOnce(true);
    const mockUpdate = jest.fn();
    asMock(getLocationSrv).mockReturnValueOnce({ update: mockUpdate });

    renderComponent();
    const search = screen.getByLabelText(SEARCH_BUTTON_LABEL);
    fireEvent.click(search);

    expect(mockUpdate).toHaveBeenCalled();
    expect(mockUpdate).toHaveBeenCalledWith({ query: { search: 'open' }, partial: true });
  });
});
