import { reducerTester } from '../../../../test/core/redux/reducerTester';
import { GroupState } from '../../../types';
import { initialState, groupLoaded, groupReducer, setGroupName } from './reducers';

describe('groupReducer', () => {
  describe('when groupLoaded is dispatched', () => {
    it('then state should be correct', () => {
      reducerTester<GroupState>()
        .givenReducer(groupReducer, { ...initialState })
        .whenActionIsDispatched(groupLoaded({ id: 1, name: 'A group' }))
        .thenStateShouldEqual({
          group: { id: 1, name: 'A group' },
        });
    });
  });

  describe('when setGroupName is dispatched', () => {
    it('then state should be correct', () => {
      reducerTester<GroupState>()
        .givenReducer(groupReducer, { ...initialState, group: { id: 1, name: 'A group' } })
        .whenActionIsDispatched(setGroupName('New Name'))
        .thenStateShouldEqual({
          group: { id: 1, name: 'New Name' },
        });
    });
  });
});
