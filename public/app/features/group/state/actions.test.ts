import { updateGroup } from './actions';
import { updateConfigurationSubtitle } from 'app/core/actions';
import { thunkTester } from 'test/core/thunk/thunkTester';

const setup = () => {
  const initialState = {
    group: {
      group: {
        id: 1,
        name: 'New Group Name',
      },
    },
  };

  return {
    initialState,
  };
};

describe('updateGroup', () => {
  describe('when updateGroup thunk is dispatched', () => {
    const getMock = jest.fn().mockResolvedValue({ id: 1, name: 'New Group Name' });
    const putMock = jest.fn().mockResolvedValue({ id: 1, name: 'New Group Name' });
    const backendSrvMock: any = {
      get: getMock,
      put: putMock,
    };

    it('then it should dispatch updateConfigurationSubtitle', async () => {
      const { initialState } = setup();

      const dispatchedActions = await thunkTester(initialState)
        .givenThunk(updateGroup)
        .whenThunkIsDispatched({ getBackendSrv: () => backendSrvMock });

      expect(dispatchedActions[0].type).toEqual(updateConfigurationSubtitle.type);
      expect(dispatchedActions[0].payload).toEqual(initialState.group.group.name);
    });
  });
});
