import React from 'react';
import { shallow } from 'enzyme';
import GroupProfile, { Props } from './GroupProfile';

const setup = () => {
  const props: Props = {
    groupName: 'Main group',
    onSubmit: jest.fn(),
  };

  return shallow(<GroupProfile {...props} />);
};

describe('Render', () => {
  it('should render component', () => {
    const wrapper = setup();

    expect(wrapper).toMatchSnapshot();
  });
});
