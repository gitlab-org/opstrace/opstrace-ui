import React, { PureComponent } from 'react';
import { hot } from 'react-hot-loader';
import { connect } from 'react-redux';
import { NavModel } from '@grafana/data';

import Page from 'app/core/components/Page/Page';
import GroupProfile from './GroupProfile';
import SharedPreferences from 'app/core/components/SharedPreferences/SharedPreferences';
import { loadGroup, updateGroup } from './state/actions';
import { Group, StoreState } from 'app/types';
import { getNavModel } from 'app/core/selectors/navModel';
import { setGroupName } from './state/reducers';
import { VerticalGroup } from '@grafana/ui';

export interface Props {
  navModel: NavModel;
  group: Group;
  loadGroup: typeof loadGroup;
  setGroupName: typeof setGroupName;
  updateGroup: typeof updateGroup;
}

export class GroupDetailsPage extends PureComponent<Props> {
  async componentDidMount() {
    await this.props.loadGroup();
  }

  onUpdateGroup = (groupName: string) => {
    this.props.setGroupName(groupName);
    this.props.updateGroup();
  };

  render() {
    const { navModel, group } = this.props;
    const isLoading = Object.keys(group).length === 0;

    return (
      <Page navModel={navModel}>
        <Page.Contents isLoading={isLoading}>
          {!isLoading && (
            <VerticalGroup>
              <GroupProfile onSubmit={this.onUpdateGroup} groupName={group.name} />
              <SharedPreferences resourceUri="group" />
            </VerticalGroup>
          )}
        </Page.Contents>
      </Page>
    );
  }
}

function mapStateToProps(state: StoreState) {
  return {
    navModel: getNavModel(state.navIndex, 'group-settings'),
    group: state.group.group,
  };
}

const mapDispatchToProps = {
  loadGroup,
  setGroupName,
  updateGroup,
};

export default hot(module)(connect(mapStateToProps, mapDispatchToProps)(GroupDetailsPage));
