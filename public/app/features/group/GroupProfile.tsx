import React, { FC } from 'react';
import { Input, Field, FieldSet, Button, Form } from '@grafana/ui';

export interface Props {
  groupName: string;
  onSubmit: (groupName: string) => void;
}

interface FormDTO {
  groupName: string;
}

const GroupProfile: FC<Props> = ({ onSubmit, groupName }) => {
  return (
    <Form defaultValues={{ groupName }} onSubmit={({ groupName }: FormDTO) => onSubmit(groupName)}>
      {({ register }) => (
        <FieldSet label="Group profile">
          <Field label="Group name">
            <Input name="groupName" type="text" ref={register({ required: true })} />
          </Field>

          <Button type="submit">Update group name</Button>
        </FieldSet>
      )}
    </Form>
  );
};

export default GroupProfile;
