type VueComponent = any;

declare module '@gitlab/ui' {
  let GlButton: VueComponent;
  let GlButtonGroup: VueComponent;
  let GlDisclosureDropdown: VueComponent;
  let GlFormSelect: VueComponent;
}
declare module '@gitlab/ui/dist/config';
