# Text Panel -  Native Plugin

The Text Panel is **included** with GitLab Observability UI.

The Text Panel is a very simple panel that displays text. The source text is written in the Markdown syntax meaning you can format the text. Read [GitLab's Markdown Guide](https://about.gitlab.com/handbook/markdown-guide/) to learn more.
