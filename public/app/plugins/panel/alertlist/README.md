# Alert List Panel - Native plugin

This Alert List panel is **included** with GitLab Observability UI.

The Alert List panel allows you to display alerts on a dashboard. The list can be configured to show either the current state of your alerts or recent alert state changes.