# Table Panel -  Native Plugin

The Table Panel is **included** with GitLab Observability UI.

The table panel is very flexible, supporting both multiple modes for time series as well as for table, annotation and raw JSON data. It also provides date formatting and value formatting and coloring options.