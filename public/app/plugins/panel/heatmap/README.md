# Heatmap Panel -  Native Plugin

The Heatmap panel allows you to view histograms over time and is **included** with GitLab Observability UI.