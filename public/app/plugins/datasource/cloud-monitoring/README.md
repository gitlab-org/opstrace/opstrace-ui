# Google Cloud Monitoring Data Source - Native Plugin (formerly named Stackdriver)

GitLab Observability UI ships with built-in support for Google Cloud Monitoring. You just have to add it as a data source and you will be ready to build dashboards for your Cloud Monitoring metrics.
