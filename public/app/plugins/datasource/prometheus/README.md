# Prometheus Data Source -  Native Plugin

GitLab Observability UI ships with **built in** support for Prometheus, the open-source service monitoring system and time series database.
