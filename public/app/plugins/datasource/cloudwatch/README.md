# CloudWatch Data Source -  Native Plugin

GitLab Observability UI ships with **built in** support for CloudWatch. You just have to add it as a data source and you will be ready to build dashboards for your CloudWatch metrics.