import React from 'react';
import { shallow } from 'enzyme';
import OrgActionBar, { Props } from './GroupActionBar';

const setup = (propOverrides?: object) => {
  const props: Props = {
    searchQuery: '',
    setSearchQuery: jest.fn(),
    linkButton: { href: 'some/url', title: 'test', target: '_blank' },
  };

  Object.assign(props, propOverrides);

  return shallow(<OrgActionBar {...props} />);
};

describe('Render', () => {
  it('should render component', () => {
    const wrapper = setup();

    expect(wrapper).toMatchSnapshot();
  });

  it('should render component without link button', () => {
    const wrapper = setup({ linkButton: undefined });

    expect(wrapper).toMatchSnapshot();
  });
});
