import { reducerTester } from '../../../test/core/redux/reducerTester';
import { navIndexReducer, updateNavIndex, updateConfigurationSubtitle } from './navModel';
import { NavIndex } from '@grafana/data';

describe('navModelReducer', () => {
  describe('when updateNavIndex is dispatched', () => {
    it('then state should be correct', () => {
      reducerTester<NavIndex>()
        .givenReducer(navIndexReducer, {})
        .whenActionIsDispatched(
          updateNavIndex({
            id: 'parent',
            text: 'Some Text',
            children: [
              {
                id: 'child',
                text: 'Child',
              },
            ],
          })
        )
        .thenStateShouldEqual({
          child: {
            id: 'child',
            text: 'Child',
            parentItem: {
              id: 'parent',
              text: 'Some Text',
              children: [
                {
                  id: 'child',
                  text: 'Child',
                },
              ],
            },
          },
        });
    });
  });

  describe('when updateConfigurationSubtitle is dispatched', () => {
    it('then state should be correct', () => {
      const originalCfg = { id: 'cfg', subTitle: 'Group: Group 1', text: 'Configuration' };
      const datasources = { id: 'datasources', text: 'Data Sources' };
      const users = { id: 'users', text: 'Users' };
      const teams = { id: 'teams', text: 'Teams' };
      const plugins = { id: 'plugins', text: 'Plugins' };
      const groupsettings = { id: 'group-settings', text: 'Preferences' };
      const apikeys = { id: 'apikeys', text: 'API Keys' };

      const initialState = {
        cfg: { ...originalCfg, children: [datasources, users, teams, plugins, groupsettings, apikeys] },
        datasources: { ...datasources, parentItem: originalCfg },
        users: { ...users, parentItem: originalCfg },
        teams: { ...teams, parentItem: originalCfg },
        plugins: { ...plugins, parentItem: originalCfg },
        'group-settings': { ...groupsettings, parentItem: originalCfg },
        apikeys: { ...apikeys, parentItem: originalCfg },
      };

      const newGroupName = 'Group 2';
      const subTitle = `Group: ${newGroupName}`;
      const newCfg = { ...originalCfg, subTitle };
      const expectedState = {
        cfg: { ...newCfg, children: [datasources, users, teams, plugins, groupsettings, apikeys] },
        datasources: { ...datasources, parentItem: newCfg },
        users: { ...users, parentItem: newCfg },
        teams: { ...teams, parentItem: newCfg },
        plugins: { ...plugins, parentItem: newCfg },
        'group-settings': { ...groupsettings, parentItem: newCfg },
        apikeys: { ...apikeys, parentItem: newCfg },
      };

      reducerTester<NavIndex>()
        .givenReducer(navIndexReducer, { ...initialState })
        .whenActionIsDispatched(updateConfigurationSubtitle(newGroupName))
        .thenStateShouldEqual(expectedState);
    });
  });
});
