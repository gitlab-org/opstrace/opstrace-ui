import React from 'react';
import { Story } from '@storybook/react';
import { GlDisclosureDropdown, GlDisclosureDropdownProps } from './GlDisclosureDropdown';
import { mockGroups, mockGroupsCustomItem, mockItems, mockItemsCustomItem } from './mock_data';
/**
 * Porting of https://gitlab.com/gitlab-org/gitlab-ui/-/blob/main/src/components/base/new_dropdowns/disclosure/disclosure_dropdown.stories.js
 */
export default {
  title: 'GitLab UI/GlDisclosureDropdown',
  component: GlDisclosureDropdown,
  argTypes: {},
};

export const Default: Story<GlDisclosureDropdownProps> = (args, { argTypes = {} }) => {
  return (
    <GlDisclosureDropdown
      items={mockItems}
      icon="ellipsis_v"
      noCaret
      textSrOnly
      toggleText="Disclosure"
      toggleId="custom-id"
    ></GlDisclosureDropdown>
  );
};

export const CustomListItem = () => {
  return (
    <GlDisclosureDropdown
      items={mockItemsCustomItem}
      icon="ellipsis_v"
      noCaret
      textSrOnly
      toggleText="Disclosure"
      toggleId="custom-id"
      $scopedSlots={{
        'list-item': function ListItem({ item }) {
          return (
            <a
              className="gl-display-flex gl-align-items-center gl-justify-content-space-between gl-hover-text-gray-900 gl-hover-text-decoration-none gl-text-gray-900"
              href={item.href}
              {...item.extraAttrs}
            >
              {/* TODO replace with badge */}
              {`${item.text} - ${item.count || 0}`}
            </a>
          );
        },
      }}
    ></GlDisclosureDropdown>
  );
};

export const GroupedItems = () => (
  <GlDisclosureDropdown items={mockGroups} icon="plus-square-o" textSrOnly toggleText="Create new" />
);

export const CustomGroupsAndItems = () => (
  <GlDisclosureDropdown
    items={mockGroupsCustomItem}
    toggleText="Merge requests"
    $scopedSlots={{
      'group-label': function GroupLabel({ group }: any) {
        {
          /* TODO replace with badge */
        }
        return `${group.name} ${group.items.reduce((acc: number, item: { count: number }) => acc + item.count, 0)}`;
      },
      'list-item': function ListItem({ item }: any) {
        return (
          <a
            className="gl-display-flex gl-align-items-center gl-justify-content-space-between gl-hover-text-gray-900 gl-hover-text-decoration-none gl-text-gray-900"
            href={item.href}
            {...item.extraAttrs}
          >
            {/* TODO replace with badge */}
            {`${item.text} - ${item.count || 0}`}
          </a>
        );
      },
    }}
  ></GlDisclosureDropdown>
);

export const MiscContent = () => (
  <GlDisclosureDropdown icon="doc-text" toggleText="Miscellaneous content" textSrOnly>
    <div className="gl-p-3">
      A disclosure dropdown is a button that toggles a panel containing a list of items and/or links.
    </div>
  </GlDisclosureDropdown>
);
