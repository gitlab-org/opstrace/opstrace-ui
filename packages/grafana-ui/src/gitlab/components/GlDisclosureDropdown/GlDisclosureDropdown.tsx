import { applyVueInReact } from 'vuereact-combined';
import React, { HTMLAttributes, ComponentType } from 'react';
import { GlDisclosureDropdown as _GlDisclosureDropdown } from '@gitlab/ui';
import { normaliseProps, resetVueWrapperStyle } from '../utils';
import { VueScopedSlotsProps } from '../types';

type Item = {
  text?: string;
  href?: string;
  action?: () => void;
  extraAttrs?: Object;
  wrapperClass?: string;
};

type Group = {
  name?: string;
  items?: Item[];
};

interface GlDisclosureDropdownAttrs extends HTMLAttributes<HTMLDivElement> {
  items?: Item[] | Group[];
  toggleText?: string;
  textSrOnly?: boolean;
  category?: string;
  variant?: string;
  size?: string;
  icon?: string;
  disabled?: boolean;
  loading?: boolean;
  toggleId?: string;
  toggleClass?: string;
  noCaret?: boolean;
  placement?: string;
  toggleAriaLabelledBy?: string;
  listAriaLabelledBy?: string;
}

export type GlDisclosureDropdownProps = GlDisclosureDropdownAttrs | VueScopedSlotsProps<'list-item' | 'group-label'>;

const GlDisclosureDropdownInternal = applyVueInReact(_GlDisclosureDropdown, resetVueWrapperStyle);
export const GlDisclosureDropdown: ComponentType<GlDisclosureDropdownProps> = (props) => (
  <GlDisclosureDropdownInternal {...normaliseProps(props)} />
);
