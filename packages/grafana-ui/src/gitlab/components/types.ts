import { ReactNode } from 'react';

type ScopedSlot = (context: any) => ReactNode;
export type VueScopedSlotsProps<S extends string> = {
  $scopedSlots?: Partial<Record<S, ScopedSlot>>;
};
