import { applyVueInReact } from 'vuereact-combined';
import React, { ButtonHTMLAttributes, ComponentType } from 'react';
import { GlButton as _GlButton } from '@gitlab/ui';
import { normaliseProps, resetVueWrapperStyle } from '../utils';

/**
 * GitLab UI Constants: gitlab.com/gitlab-org/gitlab-ui/-/blob/main/src/utils/constants.js
 */
export enum GlButtonCategory {
  primary = 'primary',
  secondary = 'secondary',
  tertiary = 'tertiary',
}

export enum GlButtonVariant {
  default = 'default',
  confirm = 'confirm',
  danger = 'danger',
  dashed = 'dashed',
  link = 'link',
  /**
   * The "reset" variant can be used when customization of GlButton styles is required
   * (e.g. for the "close" button in GlLabel).
   * It should be used sparingly and only when other approaches fail.
   * Prefer supported variants where ever possible.
   */
  // reset: 'gl-reset',
}

export enum GlButtonSize {
  small = 'small',
  medium = 'medium',
}

const GlButtonInternal = applyVueInReact(_GlButton, resetVueWrapperStyle);

export const GlButton: ComponentType<GlButtonProps> = (props) => <GlButtonInternal {...normaliseProps(props)} />;

/**
 * TODO add validation for available icons from https://gitlab-org.gitlab.io/gitlab-svgs/
 * const data = require('@gitlab/svgs/dist/icons.json');
  const { icons } = data;
 */

export interface GlButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  variant?: GlButtonVariant;
  icon?: string;
  category?: GlButtonCategory;
  size?: GlButtonSize;
  label?: boolean;
  selected?: boolean;
  loading?: boolean;
  buttonTextClasses?: string;
  disabled?: boolean;

  /**
   * Block button is supposed to take up the whole width of the container width.
   * This does not work correctly with vuereact-combined as it's wrapped in an intermediate div,
   * so it results in taking up the all space in the wrapping div.
   *
   * The solution would be to change the style of the wrapping div with `componentWrapAttrs`
   * and set its width.
   *
   * block?: boolean;
   */

  /**
   * Links are broken.
   *
   * withLink?: boolean;
   * target?: string;
   * href?: string;
   */
}
