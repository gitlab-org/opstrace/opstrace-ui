import React from 'react';
import { Story } from '@storybook/react';
import { GlButton, GlButtonProps, GlButtonSize, GlButtonCategory, GlButtonVariant } from './GlButton';

/**
 * Porting of https://gitlab.com/gitlab-org/gitlab-ui/-/blob/main/src/components/base/button/button.stories.js
 */
export default {
  title: 'GitLab UI/GlButton',
  component: GlButton,
  argTypes: {},
};

export const Default: Story<GlButtonProps> = (args, { argTypes = {} }) => {
  return <GlButton onClick={() => console.log('clicked')}>This is a button</GlButton>;
};

export const IconButton: Story<GlButtonProps> = (args, { argTypes = {} }) => {
  return (
    <GlButton
      icon="star-o"
      variant={GlButtonVariant.danger}
      category={GlButtonCategory.primary}
      aria-label="Star icon button"
    >
      This is an icon button
    </GlButton>
  );
};

export const LoadingButton: Story<GlButtonProps> = (args, { argTypes = {} }) => {
  return <GlButton loading>This is a loading button</GlButton>;
};

export const IconButtonWithOverflowedText: Story<GlButtonProps> = (args, { argTypes = {} }) => {
  return (
    <GlButton icon="star-o" style={{ width: '100px' }} aria-label="Star icon button">
      Apply suggestion
    </GlButton>
  );
};

export const BorderlessTertiary: Story<GlButtonProps> = (args, { argTypes = {} }) => {
  return <GlButton category={GlButtonCategory.tertiary}>Default borderless</GlButton>;
};

export const LabelButton: Story<GlButtonProps> = (args, { argTypes = {} }) => {
  return (
    <GlButton label disabled>
      Label
    </GlButton>
  );
};

export const AllVariantsAndCategories: Story<GlButtonProps> = (args, { argTypes = {} }) => {
  // Cannot loop through the Object.values as Storybook adds some random values to exported const/enum
  // https://github.com/storybookjs/storybook/issues/17407
  const categories = [GlButtonCategory.primary, GlButtonCategory.secondary, GlButtonCategory.tertiary];
  const variants = [
    GlButtonVariant.default,
    GlButtonVariant.confirm,
    GlButtonVariant.danger,
    GlButtonVariant.dashed,
    GlButtonVariant.link,
  ];
  return (
    <div style={{ display: 'grid', gridTemplateColumns: 'repeat(3, 150px)', rowGap: '8px', textAlign: 'center' }}>
      {variants.map((variant) => {
        return categories.map((category) => {
          return (
            <GlButton category={category} variant={variant} key={`${category}_${variant}`}>
              {`${category} ${variant}`}
            </GlButton>
          );
        });
      })}
    </div>
  );
};

export const Sizes: Story<GlButtonProps> = (args, { argTypes = {} }) => {
  return (
    <div>
      <GlButton size={GlButtonSize.small}>Small button</GlButton>
      <GlButton size={GlButtonSize.medium}>Default Medium button</GlButton>
    </div>
  );
};
