/**
 * Convert React standard props to Vue's
 */
export const normaliseProps = (props: any) => {
  const { onClick, ...rest } = props;
  const hasEventsHandler = onClick !== undefined;
  return {
    ...rest,
    ...(hasEventsHandler && {
      on: {
        //TODO add other events
        click: onClick,
      },
    }),
  };
};

/**
 * Ref: https://github.com/devilwjp/vuereact-combined#%E9%9C%80%E8%A6%81%E6%B3%A8%E6%84%8F%E7%9A%84%E5%8C%85%E5%9B%8A%E6%80%A7%E9%97%AE%E9%A2%98
 */
export const resetVueWrapperStyle = {
  // Inner wrapper component
  react: {
    componentWrapAttrs: {
      style: {},
    },
    slotWrapAttrs: {
      style: {},
    },
  },
  // Outer wrapper component
  vue: {
    componentWrapAttrs: {
      style: {},
    },
    slotWrapAttrs: {
      style: {},
    },
  },
};
