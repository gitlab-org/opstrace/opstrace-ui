import { applyVueInReact } from 'vuereact-combined';
import React, { ComponentType, HTMLAttributes } from 'react';
import { GlFormSelect as _GlFormSelect } from '@gitlab/ui';
import { normaliseProps, resetVueWrapperStyle } from '../utils';
import { css, cx } from 'emotion';

export enum GlFormSelectSize {
  xs = 'xs',
  sm = 'sm',
  md = 'md',
  lg = 'lg',
  xl = 'xl',
}

const GlFormSelectInternal = applyVueInReact(_GlFormSelect, resetVueWrapperStyle);

export const GlFormSelect: ComponentType<GlFormSelectProps> = ({ className, ...props }) => {
  const model =
    props.onValueChanged || props.selected
      ? {
          $model: {
            value: props.selected,
            setter: (value: any) => props.onValueChanged && props.onValueChanged(value),
          },
        }
      : {};
  return <GlFormSelectInternal {...normaliseProps(props)} {...model} className={`${cx(styleOverride, className)}`} />;
};

type Value = string | number;
export interface GlFormSelectProps extends HTMLAttributes<HTMLDivElement> {
  size?: GlFormSelectSize;

  disabled?: boolean;

  selected?: Value;

  onValueChanged?: (value: Value) => void;

  /**
   * See https://bootstrap-vue.org/docs/components/form-select
   * for more supported options
   */
  options?: Array<{ value: Value; text: string; disabled?: boolean }>;

  /**
   * true: valid state
   * false: invalid state
   * undefined: default state
   */
  state?: boolean;

  /**
   * Multiple Select looks weird.
   *
   * // multiple?: boolean;
   */
}

/**
 *  Need to manually set the background-image as it looks broken otherwise
 */
const styleOverride = css`
  &,
  &.is-invalid {
    background-image: url('data:image/svg+xml,%3Csvg width="8" height="5" viewBox="0 0 8 5" fill="none" xmlns="http://www.w3.org/2000/svg"%3E%3Cpath fill-rule="evenodd" clip-rule="evenodd" d="M0.21967 0.21967C0.512563 -0.0732232 0.987437 -0.0732232 1.28033 0.21967L4 2.93934L6.71967 0.21967C7.01256 -0.073223 7.48744 -0.0732229 7.78033 0.21967C8.07322 0.512563 8.07322 0.987437 7.78033 1.28033L4.53033 4.53033C4.23744 4.82322 3.76256 4.82322 3.46967 4.53033L0.21967 1.28033C-0.0732233 0.987437 -0.0732233 0.512563 0.21967 0.21967Z" fill="%23666666"/%3E%3C/svg%3E%0A');
  }
`;
