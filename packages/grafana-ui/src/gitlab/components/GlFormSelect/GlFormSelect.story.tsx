import React from 'react';
import { GlFormSelect, GlFormSelectSize } from './GlFormSelect';

const formSelectOptions = [
  { value: 'Pizza', text: 'Pizza' },
  { value: 'Tacos', text: 'Tacos' },
  { value: 'Burger', text: 'Burger' },
];

export default {
  title: 'GitLab UI/GlFormSelect',
  component: GlFormSelect,
  argTypes: {},
};

export const Default = () => {
  return (
    <GlFormSelect
      options={formSelectOptions}
      selected={formSelectOptions[1].value}
      onValueChanged={(e) => console.log('onValueChanged', e)}
    />
  );
};

export const CustomStyle = () => {
  return (
    <GlFormSelect
      style={{
        backgroundColor: 'red',
      }}
      options={formSelectOptions}
      selected={formSelectOptions[1].value}
    />
  );
};

export const Disabled = () => {
  return <GlFormSelect options={formSelectOptions} selected={formSelectOptions[0].value} disabled />;
};

export const Invalid = () => {
  return <GlFormSelect options={formSelectOptions} selected={formSelectOptions[0].value} state={false} />;
};

export const Size = () => {
  return (
    <div>
      {Object.values(GlFormSelectSize).map((size) => (
        <div key={size}>
          <GlFormSelect
            options={[{ text: 'A form select option with a very looooooooong label', value: 1 }]}
            selected={1}
            size={size}
          />
        </div>
      ))}
    </div>
  );
};
