import React from 'react';
import { Story } from '@storybook/react';
import { GlButtonGroup, GlButtonGroupProps } from './GlButtonGroup';
import { GlButton } from '../GlButton/GlButton';

/**
 * Porting of https://gitlab.com/gitlab-org/gitlab-ui/-/blob/main/src/components/base/button_group/button_group.stories.js
 */

export default {
  title: 'GitLab UI/GlButtonGroup',
  component: GlButtonGroup,
  argTypes: {},
};

export const Default: Story<GlButtonGroupProps> = () => {
  return (
    <GlButtonGroup>
      <GlButton>Button 1</GlButton>
      <GlButton>Button 2</GlButton>
      <GlButton>Button 2</GlButton>
    </GlButtonGroup>
  );
};
