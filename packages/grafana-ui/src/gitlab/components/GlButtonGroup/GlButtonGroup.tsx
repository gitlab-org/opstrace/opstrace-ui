import { applyVueInReact } from 'vuereact-combined';
import React, { HTMLAttributes, ComponentType } from 'react';
import { GlButtonGroup as _GlButtonGroup } from '@gitlab/ui';
import { normaliseProps, resetVueWrapperStyle } from '../utils';
import { css, cx } from 'emotion';

export interface GlButtonGroupProps extends HTMLAttributes<HTMLDivElement> {}

const GlButtonGroupInternal = applyVueInReact(_GlButtonGroup, {
  ...resetVueWrapperStyle,
  react: {
    componentWrapAttrs: {
      style: {},
      /**
       * Need to forward the enclosing .btn-group class (which is set by gitlab/ui) to the
       * component wrapper so that the layout is correct
       */
      class: 'btn-group',
    },
  },
});

/**
 *  Need to manually set the border as the wrapper components
 *  break some gitlab/ui css
 */
const styleOverride = css`
  div[data-use-vue-component-wrap] {
    :not(:first-of-type) {
      > button {
        border-top-left-radius: 0;
        border-bottom-left-radius: 0;
        margin-left: -1px;
      }
    }

    :not(:last-of-type) {
      > button {
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
      }
    }
  }
`;
export const GlButtonGroup: ComponentType<GlButtonGroupProps> = ({ className, ...props }) => {
  return <GlButtonGroupInternal {...normaliseProps(props)} className={`${cx(styleOverride, className)}`} />;
};
