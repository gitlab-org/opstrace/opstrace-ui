// @gitlab/ui setup
import setConfigs from '@gitlab/ui/dist/config';
import '@gitlab/ui/dist/index.css';
import '@gitlab/ui/dist/utility_classes.css';
setConfigs();
