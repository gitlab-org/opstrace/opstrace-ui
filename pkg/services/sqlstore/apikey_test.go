//go:build integration
// +build integration

package sqlstore

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/models"
)

func mockTimeNow() {
	var timeSeed int64
	timeNow = func() time.Time {
		loc := time.FixedZone("MockZoneUTC-5", -5*60*60)
		fakeNow := time.Unix(timeSeed, 0).In(loc)
		timeSeed++
		return fakeNow
	}
}

func resetTimeNow() {
	timeNow = time.Now
}

func TestApiKeyDataAccess(t *testing.T) {
	mockTimeNow()
	defer resetTimeNow()

	t.Run("Testing API Key data access", func(t *testing.T) {
		InitTestDB(t)

		t.Run("Given saved api key", func(t *testing.T) {
			cmd := models.AddApiKeyCommand{GroupId: 1, Name: "hello", Key: "asd"}
			err := AddApiKey(&cmd)
			assert.Nil(t, err)

			t.Run("Should be able to get key by name", func(t *testing.T) {
				query := models.GetApiKeyByNameQuery{KeyName: "hello", GroupId: 1}
				err = GetApiKeyByName(&query)

				assert.Nil(t, err)
				assert.NotNil(t, query.Result)
			})
		})

		t.Run("Add non expiring key", func(t *testing.T) {
			cmd := models.AddApiKeyCommand{GroupId: 1, Name: "non-expiring", Key: "asd1", SecondsToLive: 0}
			err := AddApiKey(&cmd)
			assert.Nil(t, err)

			query := models.GetApiKeyByNameQuery{KeyName: "non-expiring", GroupId: 1}
			err = GetApiKeyByName(&query)
			assert.Nil(t, err)

			assert.Nil(t, query.Result.Expires)
		})

		t.Run("Add an expiring key", func(t *testing.T) {
			// expires in one hour
			cmd := models.AddApiKeyCommand{GroupId: 1, Name: "expiring-in-an-hour", Key: "asd2", SecondsToLive: 3600}
			err := AddApiKey(&cmd)
			assert.Nil(t, err)

			query := models.GetApiKeyByNameQuery{KeyName: "expiring-in-an-hour", GroupId: 1}
			err = GetApiKeyByName(&query)
			assert.Nil(t, err)

			assert.True(t, *query.Result.Expires >= timeNow().Unix())

			// timeNow() has been called twice since creation; once by AddApiKey and once by GetApiKeyByName
			// therefore two seconds should be subtracted by next value returned by timeNow()
			// that equals the number by which timeSeed has been advanced
			then := timeNow().Add(-2 * time.Second)
			expected := then.Add(1 * time.Hour).UTC().Unix()
			assert.Equal(t, *query.Result.Expires, expected)
		})

		t.Run("Add a key with negative lifespan", func(t *testing.T) {
			// expires in one day
			cmd := models.AddApiKeyCommand{GroupId: 1, Name: "key-with-negative-lifespan", Key: "asd3", SecondsToLive: -3600}
			err := AddApiKey(&cmd)
			assert.EqualError(t, err, models.ErrInvalidApiKeyExpiration.Error())

			query := models.GetApiKeyByNameQuery{KeyName: "key-with-negative-lifespan", GroupId: 1}
			err = GetApiKeyByName(&query)
			assert.EqualError(t, err, "invalid API key")
		})

		t.Run("Add keys", func(t *testing.T) {
			// never expires
			cmd := models.AddApiKeyCommand{GroupId: 1, Name: "key1", Key: "key1", SecondsToLive: 0}
			err := AddApiKey(&cmd)
			assert.Nil(t, err)

			// expires in 1s
			cmd = models.AddApiKeyCommand{GroupId: 1, Name: "key2", Key: "key2", SecondsToLive: 1}
			err = AddApiKey(&cmd)
			assert.Nil(t, err)

			// expires in one hour
			cmd = models.AddApiKeyCommand{GroupId: 1, Name: "key3", Key: "key3", SecondsToLive: 3600}
			err = AddApiKey(&cmd)
			assert.Nil(t, err)

			// advance mocked getTime by 1s
			timeNow()

			query := models.GetApiKeysQuery{GroupId: 1, IncludeExpired: false}
			err = GetApiKeys(&query)
			assert.Nil(t, err)

			for _, k := range query.Result {
				if k.Name == "key2" {
					t.Fatalf("key2 should not be there")
				}
			}

			query = models.GetApiKeysQuery{GroupId: 1, IncludeExpired: true}
			err = GetApiKeys(&query)
			assert.Nil(t, err)

			found := false
			for _, k := range query.Result {
				if k.Name == "key2" {
					found = true
				}
			}
			assert.True(t, found)
		})
	})
}

func TestApiKeyErrors(t *testing.T) {
	mockTimeNow()
	defer resetTimeNow()

	t.Run("Testing API Duplicate Key Errors", func(t *testing.T) {
		InitTestDB(t)
		t.Run("Given saved api key", func(t *testing.T) {
			cmd := models.AddApiKeyCommand{GroupId: 0, Name: "duplicate", Key: "asd"}
			err := AddApiKey(&cmd)
			assert.Nil(t, err)

			t.Run("Add API Key with existing Org ID and Name", func(t *testing.T) {
				cmd := models.AddApiKeyCommand{GroupId: 0, Name: "duplicate", Key: "asd"}
				err = AddApiKey(&cmd)
				assert.EqualError(t, err, models.ErrDuplicateApiKey.Error())
			})
		})
	})
}
