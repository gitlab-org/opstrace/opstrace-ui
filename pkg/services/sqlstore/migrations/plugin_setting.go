package migrations

import . "gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/services/sqlstore/migrator"

func addAppSettingsMigration(mg *Migrator) {
	pluginSettingTable := Table{
		Name: "plugin_setting",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "org_id", Type: DB_BigInt, Nullable: true},
			{Name: "plugin_id", Type: DB_NVarchar, Length: 190, Nullable: false},
			{Name: "enabled", Type: DB_Bool, Nullable: false},
			{Name: "pinned", Type: DB_Bool, Nullable: false},
			{Name: "json_data", Type: DB_Text, Nullable: true},
			{Name: "secure_json_data", Type: DB_Text, Nullable: true},
			{Name: "created", Type: DB_DateTime, Nullable: false},
			{Name: "updated", Type: DB_DateTime, Nullable: false},
		},
		Indices: []*Index{
			{Cols: []string{"org_id", "plugin_id"}, Type: UniqueIndex},
		},
	}

	mg.AddMigration("create plugin_setting table", NewAddTableMigration(pluginSettingTable))

	//-------  indexes ------------------
	addTableIndicesMigrations(mg, "v1", pluginSettingTable)

	// add column to store installed version
	mg.AddMigration("Add column plugin_version to plugin_settings", NewAddColumnMigration(pluginSettingTable, &Column{
		Name: "plugin_version", Type: DB_NVarchar, Nullable: true, Length: 50,
	}))

	mg.AddMigration("Update plugin_setting table charset", NewTableCharsetMigration("plugin_setting", []*Column{
		{Name: "plugin_id", Type: DB_NVarchar, Length: 190, Nullable: false},
		{Name: "json_data", Type: DB_Text, Nullable: true},
		{Name: "secure_json_data", Type: DB_Text, Nullable: true},
		{Name: "plugin_version", Type: DB_NVarchar, Nullable: true, Length: 50},
	}))

	// ---------------------
	// org -> group changes

	// drop indexes
	addDropAllIndicesMigrations(mg, "v1", pluginSettingTable)

	// rename table
	addTableRenameMigration(mg, "plugin_setting", "plugin_setting_v2", "v2")

	pluginSettingTableV2 := Table{
		Name: "plugin_setting",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "group_id", Type: DB_BigInt, Nullable: true},
			{Name: "plugin_id", Type: DB_NVarchar, Length: 190, Nullable: false},
			{Name: "enabled", Type: DB_Bool, Nullable: false},
			{Name: "pinned", Type: DB_Bool, Nullable: false},
			{Name: "json_data", Type: DB_Text, Nullable: true},
			{Name: "secure_json_data", Type: DB_Text, Nullable: true},
			{Name: "created", Type: DB_DateTime, Nullable: false},
			{Name: "updated", Type: DB_DateTime, Nullable: false},
			{Name: "plugin_version", Type: DB_NVarchar, Nullable: true, Length: 50},
		},
		Indices: []*Index{
			{Cols: []string{"group_id", "plugin_id"}, Type: UniqueIndex},
		},
	}

	// create v2 table
	mg.AddMigration("create plugin_setting table v2", NewAddTableMigration(pluginSettingTableV2))

	// add v2 indíces
	addTableIndicesMigrations(mg, "v2", pluginSettingTableV2)

	//------- copy data from v2 to v3 -------------------
	mg.AddMigration("copy plugin_setting_v2 v1 to v2", NewCopyTableDataMigration("plugin_setting", "plugin_setting_v2", map[string]string{
		"id":               "id",
		"group_id":         "org_id",
		"plugin_id":        "plugin_id",
		"enabled":          "enabled",
		"pinned":           "pinned",
		"json_data":        "json_data",
		"secure_json_data": "secure_json_data",
		"created":          "created",
		"updated":          "updated",
		"plugin_version":   "plugin_version",
	}))

	mg.AddMigration("Drop old table plugin_setting_v2", NewDropTableMigration("plugin_setting_v2"))
}
