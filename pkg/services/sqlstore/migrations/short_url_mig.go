package migrations

import (
	. "gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/services/sqlstore/migrator"
)

func addShortURLMigrations(mg *Migrator) {
	shortURLV1 := Table{
		Name: "short_url",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, Nullable: false, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "org_id", Type: DB_BigInt, Nullable: false},
			{Name: "uid", Type: DB_NVarchar, Length: 40, Nullable: false},
			{Name: "path", Type: DB_Text, Nullable: false},
			{Name: "created_by", Type: DB_Int, Nullable: false},
			{Name: "created_at", Type: DB_Int, Nullable: false},
			{Name: "last_seen_at", Type: DB_Int, Nullable: true},
		},
		Indices: []*Index{
			{Cols: []string{"org_id", "uid"}, Type: UniqueIndex},
		},
	}

	mg.AddMigration("create short_url table v1", NewAddTableMigration(shortURLV1))

	mg.AddMigration("add index short_url.org_id-uid", NewAddIndexMigration(shortURLV1, shortURLV1.Indices[0]))

	// ---------------------
	// org -> group changes

	// drop indexes
	addDropAllIndicesMigrations(mg, "v1", shortURLV1)

	// rename table
	addTableRenameMigration(mg, "short_url", "short_url_v2", "v2")

	shortURLV2 := Table{
		Name: "short_url",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, Nullable: false, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "group_id", Type: DB_BigInt, Nullable: false},
			{Name: "uid", Type: DB_NVarchar, Length: 40, Nullable: false},
			{Name: "path", Type: DB_Text, Nullable: false},
			{Name: "created_by", Type: DB_Int, Nullable: false},
			{Name: "created_at", Type: DB_Int, Nullable: false},
			{Name: "last_seen_at", Type: DB_Int, Nullable: true},
		},
		Indices: []*Index{
			{Cols: []string{"group_id", "uid"}, Type: UniqueIndex},
		},
	}

	// create v2 table
	mg.AddMigration("create short_url table v2", NewAddTableMigration(shortURLV2))

	// add v2 indíces
	addTableIndicesMigrations(mg, "v2", shortURLV2)

	//------- copy data from v2 to v3 -------------------
	mg.AddMigration("copy short_url v1 to v2", NewCopyTableDataMigration("short_url", "short_url_v2", map[string]string{
		"id":           "id",
		"group_id":     "org_id",
		"uid":          "uid",
		"path":         "path",
		"created_by":   "created_by",
		"created_at":   "created_at",
		"last_seen_at": "last_seen_at",
	}))

	mg.AddMigration("Drop old table short_url_v2", NewDropTableMigration("short_url_v2"))
}
