package migrations

import (
	. "gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/services/sqlstore/migrator"
)

func addOrgMigrations(mg *Migrator) {
	orgV1 := Table{
		Name: "org",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "version", Type: DB_Int, Nullable: false},
			{Name: "name", Type: DB_NVarchar, Length: 190, Nullable: false},
			{Name: "address1", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "address2", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "city", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "state", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "zip_code", Type: DB_NVarchar, Length: 50, Nullable: true},
			{Name: "country", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "billing_email", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "created", Type: DB_DateTime, Nullable: false},
			{Name: "updated", Type: DB_DateTime, Nullable: false},
		},
		Indices: []*Index{
			{Cols: []string{"name"}, Type: UniqueIndex},
		},
	}

	// add org v1
	mg.AddMigration("create org table v1", NewAddTableMigration(orgV1))
	addTableIndicesMigrations(mg, "v1", orgV1)

	orgUserV1 := Table{
		Name: "org_user",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "org_id", Type: DB_BigInt},
			{Name: "user_id", Type: DB_BigInt},
			{Name: "role", Type: DB_NVarchar, Length: 20},
			{Name: "created", Type: DB_DateTime},
			{Name: "updated", Type: DB_DateTime},
		},
		Indices: []*Index{
			{Cols: []string{"org_id"}},
			{Cols: []string{"org_id", "user_id"}, Type: UniqueIndex},
		},
	}

	//-------  org_user table -------------------
	mg.AddMigration("create org_user table v1", NewAddTableMigration(orgUserV1))
	addTableIndicesMigrations(mg, "v1", orgUserV1)

	mg.AddMigration("Update org table charset", NewTableCharsetMigration("org", []*Column{
		{Name: "name", Type: DB_NVarchar, Length: 190, Nullable: false},
		{Name: "address1", Type: DB_NVarchar, Length: 255, Nullable: true},
		{Name: "address2", Type: DB_NVarchar, Length: 255, Nullable: true},
		{Name: "city", Type: DB_NVarchar, Length: 255, Nullable: true},
		{Name: "state", Type: DB_NVarchar, Length: 255, Nullable: true},
		{Name: "zip_code", Type: DB_NVarchar, Length: 50, Nullable: true},
		{Name: "country", Type: DB_NVarchar, Length: 255, Nullable: true},
		{Name: "billing_email", Type: DB_NVarchar, Length: 255, Nullable: true},
	}))

	mg.AddMigration("Update org_user table charset", NewTableCharsetMigration("org_user", []*Column{
		{Name: "role", Type: DB_NVarchar, Length: 20},
	}))

	const migrateReadOnlyViewersToViewers = `UPDATE org_user SET role = 'Viewer' WHERE role = 'Read Only Editor'`
	mg.AddMigration("Migrate all Read Only Viewers to Viewers", NewRawSQLMigration(migrateReadOnlyViewersToViewers))

	// ---------------------
	// org -> group changes

	// drop indexes
	addDropAllIndicesMigrations(mg, "v2", orgV1)

	// rename table
	addTableRenameMigration(mg, "org", "org_v2", "v2")

	groupV1 := Table{
		Name: "grp",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "version", Type: DB_Int, Nullable: false},
			{Name: "name", Type: DB_NVarchar, Length: 190, Nullable: false},
			{Name: "address1", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "address2", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "city", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "state", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "zip_code", Type: DB_NVarchar, Length: 50, Nullable: true},
			{Name: "country", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "billing_email", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "created", Type: DB_DateTime, Nullable: false},
			{Name: "updated", Type: DB_DateTime, Nullable: false},
		},
		Indices: []*Index{
			{Cols: []string{"name"}, Type: UniqueIndex},
		},
	}

	// create group v1 table
	mg.AddMigration("create group table groupV1", NewAddTableMigration(groupV1))

	// add group v1 indíces
	addTableIndicesMigrations(mg, "v1", groupV1)

	//------- copy data from org_v2 to groupV1 -------------------
	mg.AddMigration("copy org org_v2 to groupV1", NewCopyTableDataMigration("grp", "org_v2", map[string]string{
		"id":            "id",
		"version":       "version",
		"name":          "name",
		"address1":      "address1",
		"address2":      "address2",
		"city":          "city",
		"state":         "state",
		"zip_code":      "zip_code",
		"country":       "country",
		"billing_email": "billing_email",
		"created":       "created",
		"updated":       "updated",
	}))

	mg.AddMigration("Drop old table org_v2", NewDropTableMigration("org_v2"))

	// ---------------------
	// org -> group changes

	// drop indexes
	addDropAllIndicesMigrations(mg, "v1", orgUserV1)

	// rename table
	addTableRenameMigration(mg, "org_user", "org_user_v2", "v2")

	groupUserV1 := Table{
		Name: "group_user",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "group_id", Type: DB_BigInt},
			{Name: "user_id", Type: DB_BigInt},
			{Name: "role", Type: DB_NVarchar, Length: 20},
			{Name: "created", Type: DB_DateTime},
			{Name: "updated", Type: DB_DateTime},
		},
		Indices: []*Index{
			{Cols: []string{"group_id"}},
			{Cols: []string{"group_id", "user_id"}, Type: UniqueIndex},
		},
	}

	// create group_user v1 table
	mg.AddMigration("create group_user table v1", NewAddTableMigration(groupUserV1))

	// add group_user v1 indíces
	addTableIndicesMigrations(mg, "v1", groupUserV1)

	//------- copy data from org_user_v2 to group_user v1 -------------------
	mg.AddMigration("copy org_user_v2 v2 to group_user v1", NewCopyTableDataMigration("group_user", "org_user_v2", map[string]string{
		"id":       "id",
		"group_id": "org_id",
		"user_id":  "user_id",
		"role":     "role",
		"created":  "created",
		"updated":  "updated",
	}))

	mg.AddMigration("Drop old table org_user_v2", NewDropTableMigration("org_user_v2"))

}
