package migrations

import . "gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/services/sqlstore/migrator"

func addTeamMigrations(mg *Migrator) {
	teamV1 := Table{
		Name: "team",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "name", Type: DB_NVarchar, Length: 190, Nullable: false},
			{Name: "org_id", Type: DB_BigInt},
			{Name: "created", Type: DB_DateTime, Nullable: false},
			{Name: "updated", Type: DB_DateTime, Nullable: false},
		},
		Indices: []*Index{
			{Cols: []string{"org_id"}},
			{Cols: []string{"org_id", "name"}, Type: UniqueIndex},
		},
	}

	mg.AddMigration("create team table", NewAddTableMigration(teamV1))

	//-------  indexes ------------------
	mg.AddMigration("add index team.org_id", NewAddIndexMigration(teamV1, teamV1.Indices[0]))
	mg.AddMigration("add unique index team_org_id_name", NewAddIndexMigration(teamV1, teamV1.Indices[1]))

	teamMemberV1 := Table{
		Name: "team_member",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "org_id", Type: DB_BigInt},
			{Name: "team_id", Type: DB_BigInt},
			{Name: "user_id", Type: DB_BigInt},
			{Name: "created", Type: DB_DateTime, Nullable: false},
			{Name: "updated", Type: DB_DateTime, Nullable: false},
		},
		Indices: []*Index{
			{Cols: []string{"org_id"}},
			{Cols: []string{"org_id", "team_id", "user_id"}, Type: UniqueIndex},
			{Cols: []string{"team_id"}},
		},
	}

	mg.AddMigration("create team member table", NewAddTableMigration(teamMemberV1))

	//-------  indexes ------------------
	mg.AddMigration("add index team_member.org_id", NewAddIndexMigration(teamMemberV1, teamMemberV1.Indices[0]))
	mg.AddMigration("add unique index team_member_org_id_team_id_user_id", NewAddIndexMigration(teamMemberV1, teamMemberV1.Indices[1]))
	mg.AddMigration("add index team_member.team_id", NewAddIndexMigration(teamMemberV1, teamMemberV1.Indices[2]))

	//add column email
	mg.AddMigration("Add column email to team table", NewAddColumnMigration(teamV1, &Column{
		Name: "email", Type: DB_NVarchar, Nullable: true, Length: 190,
	}))

	mg.AddMigration("Add column external to team_member table", NewAddColumnMigration(teamMemberV1, &Column{
		Name: "external", Type: DB_Bool, Nullable: true,
	}))

	mg.AddMigration("Add column permission to team_member table", NewAddColumnMigration(teamMemberV1, &Column{
		Name: "permission", Type: DB_SmallInt, Nullable: true,
	}))

	// ---------------------
	// org -> group changes

	// drop indexes
	addDropAllIndicesMigrations(mg, "v1", teamV1)

	// rename table
	addTableRenameMigration(mg, "team", "team_v2", "v2")

	teamV2 := Table{
		Name: "team",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "name", Type: DB_NVarchar, Length: 190, Nullable: false},
			{Name: "group_id", Type: DB_BigInt},
			{Name: "created", Type: DB_DateTime, Nullable: false},
			{Name: "updated", Type: DB_DateTime, Nullable: false},
			{Name: "email", Type: DB_NVarchar, Nullable: true, Length: 190},
		},
		Indices: []*Index{
			{Cols: []string{"group_id"}},
			{Cols: []string{"group_id", "name"}, Type: UniqueIndex},
		},
	}

	// create v2 table
	mg.AddMigration("create team table v2", NewAddTableMigration(teamV2))

	// add v2 indíces
	addTableIndicesMigrations(mg, "v2", teamV2)

	//------- copy data from v1 to v2 -------------------
	mg.AddMigration("copy team v1 to v2", NewCopyTableDataMigration("team", "team_v2", map[string]string{
		"id":       "id",
		"name":     "name",
		"group_id": "org_id",
		"created":  "created",
		"updated":  "updated",
		"email":    "email",
	}))

	mg.AddMigration("Drop old table team_v2", NewDropTableMigration("team_v2"))

	// ---------------------
	// org -> group changes

	// drop indexes
	addDropAllIndicesMigrations(mg, "v1", teamMemberV1)

	// rename table
	addTableRenameMigration(mg, "team_member", "team_member_v2", "v2")

	teamMemberV2 := Table{
		Name: "team_member",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "group_id", Type: DB_BigInt},
			{Name: "team_id", Type: DB_BigInt},
			{Name: "user_id", Type: DB_BigInt},
			{Name: "created", Type: DB_DateTime, Nullable: false},
			{Name: "updated", Type: DB_DateTime, Nullable: false},
			{Name: "external", Type: DB_Bool, Nullable: true},
			{Name: "permission", Type: DB_SmallInt, Nullable: true},
		},
		Indices: []*Index{
			{Cols: []string{"group_id"}},
			{Cols: []string{"group_id", "team_id", "user_id"}, Type: UniqueIndex},
			{Cols: []string{"team_id"}},
		},
	}

	// create v2 table
	mg.AddMigration("create team_member team_member v2", NewAddTableMigration(teamMemberV2))

	// add v2 indíces
	addTableIndicesMigrations(mg, "v2", teamMemberV2)

	//------- copy data from v1 to v2 -------------------
	mg.AddMigration("copy team_member v1 to v2", NewCopyTableDataMigration("team_member", "team_member_v2", map[string]string{
		"id":         "id",
		"group_id":   "org_id",
		"team_id":    "team_id",
		"user_id":    "user_id",
		"created":    "created",
		"updated":    "updated",
		"external":   "external",
		"permission": "permission",
	}))

	mg.AddMigration("Drop old table team_member_v2", NewDropTableMigration("team_member_v2"))
}
