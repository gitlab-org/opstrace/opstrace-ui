package sqlstore

import (
	"time"

	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/bus"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/models"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/setting"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/util"
)

func init() {
	bus.AddHandler("sql", GetPluginSettings)
	bus.AddHandler("sql", GetPluginSettingById)
	bus.AddHandler("sql", UpdatePluginSetting)
	bus.AddHandler("sql", UpdatePluginSettingVersion)
}

func GetPluginSettings(query *models.GetPluginSettingsQuery) error {
	sql := `SELECT group_id, plugin_id, enabled, pinned, plugin_version
					FROM plugin_setting `
	params := make([]interface{}, 0)

	if query.GroupId != 0 {
		sql += "WHERE group_id=?"
		params = append(params, query.GroupId)
	}

	sess := x.SQL(sql, params...)
	query.Result = make([]*models.PluginSettingInfoDTO, 0)
	return sess.Find(&query.Result)
}

func GetPluginSettingById(query *models.GetPluginSettingByIdQuery) error {
	pluginSetting := models.PluginSetting{GroupId: query.GroupId, PluginId: query.PluginId}
	has, err := x.Get(&pluginSetting)
	if err != nil {
		return err
	} else if !has {
		return models.ErrPluginSettingNotFound
	}
	query.Result = &pluginSetting
	return nil
}

func UpdatePluginSetting(cmd *models.UpdatePluginSettingCmd) error {
	return inTransaction(func(sess *DBSession) error {
		var pluginSetting models.PluginSetting

		exists, err := sess.Where("group_id=? and plugin_id=?", cmd.GroupId, cmd.PluginId).Get(&pluginSetting)
		if err != nil {
			return err
		}
		sess.UseBool("enabled")
		sess.UseBool("pinned")
		if !exists {
			pluginSetting = models.PluginSetting{
				PluginId:       cmd.PluginId,
				GroupId:        cmd.GroupId,
				Enabled:        cmd.Enabled,
				Pinned:         cmd.Pinned,
				JsonData:       cmd.JsonData,
				PluginVersion:  cmd.PluginVersion,
				SecureJsonData: cmd.GetEncryptedJsonData(),
				Created:        time.Now(),
				Updated:        time.Now(),
			}

			// add state change event on commit success
			sess.events = append(sess.events, &models.PluginStateChangedEvent{
				PluginId: cmd.PluginId,
				GroupId:  cmd.GroupId,
				Enabled:  cmd.Enabled,
			})

			_, err = sess.Insert(&pluginSetting)
			return err
		}
		for key, data := range cmd.SecureJsonData {
			encryptedData, err := util.Encrypt([]byte(data), setting.SecretKey)
			if err != nil {
				return err
			}

			pluginSetting.SecureJsonData[key] = encryptedData
		}

		// add state change event on commit success
		if pluginSetting.Enabled != cmd.Enabled {
			sess.events = append(sess.events, &models.PluginStateChangedEvent{
				PluginId: cmd.PluginId,
				GroupId:  cmd.GroupId,
				Enabled:  cmd.Enabled,
			})
		}

		pluginSetting.Updated = time.Now()
		pluginSetting.Enabled = cmd.Enabled
		pluginSetting.JsonData = cmd.JsonData
		pluginSetting.Pinned = cmd.Pinned
		pluginSetting.PluginVersion = cmd.PluginVersion

		_, err = sess.ID(pluginSetting.Id).Update(&pluginSetting)
		return err
	})
}

func UpdatePluginSettingVersion(cmd *models.UpdatePluginSettingVersionCmd) error {
	return inTransaction(func(sess *DBSession) error {
		_, err := sess.Exec("UPDATE plugin_setting SET plugin_version=? WHERE group_id=? AND plugin_id=?", cmd.PluginVersion, cmd.GroupId, cmd.PluginId)
		return err
	})
}
