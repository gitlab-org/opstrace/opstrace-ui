package sqlstore

import (
	"fmt"
	"strings"
	"time"

	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/bus"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/models"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/util"
)

func init() {
	bus.AddHandler("sql", AddOrgUser)
	bus.AddHandler("sql", RemoveOrgUser)
	bus.AddHandler("sql", GetOrgUsers)
	bus.AddHandler("sql", UpdateOrgUser)
}

func AddOrgUser(cmd *models.AddGroupUserCommand) error {
	return inTransaction(func(sess *DBSession) error {
		// check if user exists
		var user models.User
		if exists, err := sess.ID(cmd.UserId).Get(&user); err != nil {
			return err
		} else if !exists {
			return models.ErrUserNotFound
		}

		if res, err := sess.Query("SELECT 1 from group_user WHERE group_id=? and user_id=?", cmd.GroupId, user.Id); err != nil {
			return err
		} else if len(res) == 1 {
			return models.ErrGroupUserAlreadyAdded
		}

		if res, err := sess.Query("SELECT 1 from grp WHERE id=?", cmd.GroupId); err != nil {
			return err
		} else if len(res) != 1 {
			return models.ErrGroupNotFound
		}

		entity := models.GroupUser{
			GroupId: cmd.GroupId,
			UserId:  cmd.UserId,
			Role:    cmd.Role,
			Created: time.Now(),
			Updated: time.Now(),
		}

		_, err := sess.Insert(&entity)
		if err != nil {
			return err
		}

		var userOrgs []*models.UserGroupDTO
		sess.Table("group_user")
		sess.Join("INNER", "grp", "group_user.group_id=grp.id")
		sess.Where("group_user.user_id=? AND group_user.group_id=?", user.Id, user.GroupId)
		sess.Cols("grp.name", "group_user.role", "group_user.group_id")
		err = sess.Find(&userOrgs)

		if err != nil {
			return err
		}

		if len(userOrgs) == 0 {
			return setUsingOrgInTransaction(sess, user.Id, cmd.GroupId)
		}

		return nil
	})
}

func UpdateOrgUser(cmd *models.UpdateGroupUserCommand) error {
	return inTransaction(func(sess *DBSession) error {
		var orgUser models.GroupUser
		exists, err := sess.Where("group_id=? AND user_id=?", cmd.GroupId, cmd.UserId).Get(&orgUser)
		if err != nil {
			return err
		}

		if !exists {
			return models.ErrGroupUserNotFound
		}

		orgUser.Role = cmd.Role
		orgUser.Updated = time.Now()
		_, err = sess.ID(orgUser.Id).Update(&orgUser)
		if err != nil {
			return err
		}

		return validateOneAdminLeftInOrg(cmd.GroupId, sess)
	})
}

func GetOrgUsers(query *models.GetGroupUsersQuery) error {
	query.Result = make([]*models.GroupUserDTO, 0)

	sess := x.Table("group_user")
	sess.Join("INNER", x.Dialect().Quote("user"), fmt.Sprintf("group_user.user_id=%s.id", x.Dialect().Quote("user")))

	whereConditions := make([]string, 0)
	whereParams := make([]interface{}, 0)

	whereConditions = append(whereConditions, "group_user.group_id = ?")
	whereParams = append(whereParams, query.GroupId)

	if query.Query != "" {
		queryWithWildcards := "%" + query.Query + "%"
		whereConditions = append(whereConditions, "(email "+dialect.LikeStr()+" ? OR name "+dialect.LikeStr()+" ? OR login "+dialect.LikeStr()+" ?)")
		whereParams = append(whereParams, queryWithWildcards, queryWithWildcards, queryWithWildcards)
	}

	if len(whereConditions) > 0 {
		sess.Where(strings.Join(whereConditions, " AND "), whereParams...)
	}

	if query.Limit > 0 {
		sess.Limit(query.Limit, 0)
	}

	sess.Cols(
		"group_user.group_id",
		"group_user.user_id",
		"user.email",
		"user.name",
		"user.login",
		"group_user.role",
		"user.last_seen_at",
	)
	sess.Asc("user.email", "user.login")

	if err := sess.Find(&query.Result); err != nil {
		return err
	}

	for _, user := range query.Result {
		user.LastSeenAtAge = util.GetAgeString(user.LastSeenAt)
	}

	return nil
}

func RemoveOrgUser(cmd *models.RemoveGroupUserCommand) error {
	return inTransaction(func(sess *DBSession) error {
		// check if user exists
		var user models.User
		if exists, err := sess.ID(cmd.UserId).Get(&user); err != nil {
			return err
		} else if !exists {
			return models.ErrUserNotFound
		}

		deletes := []string{
			"DELETE FROM group_user WHERE group_id=? and user_id=?",
			"DELETE FROM dashboard_acl WHERE group_id=? and user_id = ?",
			"DELETE FROM team_member WHERE group_id=? and user_id = ?",
		}

		for _, sql := range deletes {
			_, err := sess.Exec(sql, cmd.GroupId, cmd.UserId)
			if err != nil {
				return err
			}
		}

		// validate that after delete there is at least one user with admin role in org
		if err := validateOneAdminLeftInOrg(cmd.GroupId, sess); err != nil {
			return err
		}

		// check user other orgs and update user current org
		var userOrgs []*models.UserGroupDTO
		sess.Table("group_user")
		sess.Join("INNER", "grp", "group_user.group_id=grp.id")
		sess.Where("group_user.user_id=?", user.Id)
		sess.Cols("grp.name", "group_user.role", "group_user.group_id")
		err := sess.Find(&userOrgs)

		if err != nil {
			return err
		}

		if len(userOrgs) > 0 {
			hasCurrentOrgSet := false
			for _, userOrg := range userOrgs {
				if user.GroupId == userOrg.GroupId {
					hasCurrentOrgSet = true
					break
				}
			}

			if !hasCurrentOrgSet {
				err = setUsingOrgInTransaction(sess, user.Id, userOrgs[0].GroupId)
				if err != nil {
					return err
				}
			}
		} else if cmd.ShouldDeleteOrphanedUser {
			// no other groups, delete the full user
			if err := deleteUserInTransaction(sess, &models.DeleteUserCommand{UserId: user.Id}); err != nil {
				return err
			}

			cmd.UserWasDeleted = true
		}

		return nil
	})
}

func validateOneAdminLeftInOrg(orgId int64, sess *DBSession) error {
	// validate that there is an admin user left
	res, err := sess.Query("SELECT 1 from group_user WHERE group_id=? and role='Admin'", orgId)
	if err != nil {
		return err
	}

	if len(res) == 0 {
		return models.ErrLastGroupAdmin
	}

	return err
}
