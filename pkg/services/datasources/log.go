package datasources

import (
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/infra/log"
)

var (
	plog = log.New("datasources")
)
