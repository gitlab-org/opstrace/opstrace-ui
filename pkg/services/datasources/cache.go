package datasources

import (
	"fmt"
	"time"

	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/infra/localcache"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/models"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/registry"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/services/sqlstore"
)

type CacheService interface {
	GetDatasource(datasourceID int64, user *models.SignedInUser, skipCache bool) (*models.DataSource, error)
	GetDatasourceByUID(datasourceUID string, user *models.SignedInUser, skipCache bool) (*models.DataSource, error)
}

type CacheServiceImpl struct {
	CacheService *localcache.CacheService `inject:""`
	SQLStore     *sqlstore.SQLStore       `inject:""`
}

func init() {
	registry.Register(&registry.Descriptor{
		Name:         "DatasourceCacheService",
		Instance:     &CacheServiceImpl{},
		InitPriority: registry.Low,
	})
}

func (dc *CacheServiceImpl) Init() error {
	return nil
}

func (dc *CacheServiceImpl) GetDatasource(
	datasourceID int64,
	user *models.SignedInUser,
	skipCache bool,
) (*models.DataSource, error) {
	cacheKey := idKey(datasourceID)

	if !skipCache {
		if cached, found := dc.CacheService.Get(cacheKey); found {
			ds := cached.(*models.DataSource)
			if ds.GroupId == user.GroupId {
				return ds, nil
			}
		}
	}

	plog.Debug("Querying for data source via SQL store", "id", datasourceID, "groupId", user.GroupId)
	ds, err := dc.SQLStore.GetDataSource("", datasourceID, "", user.GroupId)
	if err != nil {
		return nil, err
	}

	if ds.Uid != "" {
		dc.CacheService.Set(uidKey(ds.GroupId, ds.Uid), ds, time.Second*5)
	}
	dc.CacheService.Set(cacheKey, ds, time.Second*5)
	return ds, nil
}

func (dc *CacheServiceImpl) GetDatasourceByUID(
	datasourceUID string,
	user *models.SignedInUser,
	skipCache bool,
) (*models.DataSource, error) {
	if datasourceUID == "" {
		return nil, fmt.Errorf("can not get data source by uid, uid is empty")
	}
	if user.GroupId == 0 {
		return nil, fmt.Errorf("can not get data source by uid, groupId is missing")
	}
	uidCacheKey := uidKey(user.GroupId, datasourceUID)

	if !skipCache {
		if cached, found := dc.CacheService.Get(uidCacheKey); found {
			ds := cached.(*models.DataSource)
			if ds.GroupId == user.GroupId {
				return ds, nil
			}
		}
	}

	plog.Debug("Querying for data source via SQL store", "uid", datasourceUID, "groupId", user.GroupId)
	ds, err := dc.SQLStore.GetDataSource(datasourceUID, 0, "", user.GroupId)
	if err != nil {
		return nil, err
	}

	dc.CacheService.Set(uidCacheKey, ds, time.Second*5)
	dc.CacheService.Set(idKey(ds.Id), ds, time.Second*5)
	return ds, nil
}

func idKey(id int64) string {
	return fmt.Sprintf("ds-%d", id)
}

func uidKey(groupID int64, uid string) string {
	return fmt.Sprintf("ds-groupid-uid-%d-%s", groupID, uid)
}
