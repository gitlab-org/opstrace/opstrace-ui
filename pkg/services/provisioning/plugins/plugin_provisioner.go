package plugins

import (
	"errors"

	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/bus"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/infra/log"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/models"
)

// Provision scans a directory for provisioning config files
// and provisions the app in those files.
func Provision(configDirectory string) error {
	ap := newAppProvisioner(log.New("provisioning.plugins"))
	return ap.applyChanges(configDirectory)
}

// PluginProvisioner is responsible for provisioning apps based on
// configuration read by the `configReader`
type PluginProvisioner struct {
	log         log.Logger
	cfgProvider configReader
}

func newAppProvisioner(log log.Logger) PluginProvisioner {
	return PluginProvisioner{
		log:         log,
		cfgProvider: newConfigReader(log),
	}
}

func (ap *PluginProvisioner) apply(cfg *pluginsAsConfig) error {
	for _, app := range cfg.Apps {
		if app.GroupID == 0 && app.GroupName != "" {
			getOrgQuery := &models.GetGroupByNameQuery{Name: app.GroupName}
			if err := bus.Dispatch(getOrgQuery); err != nil {
				return err
			}
			app.GroupID = getOrgQuery.Result.Id
		} else if app.GroupID < 0 {
			app.GroupID = 1
		}

		query := &models.GetPluginSettingByIdQuery{GroupId: app.GroupID, PluginId: app.PluginID}
		err := bus.Dispatch(query)
		if err != nil {
			if !errors.Is(err, models.ErrPluginSettingNotFound) {
				return err
			}
		} else {
			app.PluginVersion = query.Result.PluginVersion
		}

		ap.log.Info("Updating app from configuration ", "type", app.PluginID, "enabled", app.Enabled)
		cmd := &models.UpdatePluginSettingCmd{
			GroupId:        app.GroupID,
			PluginId:       app.PluginID,
			Enabled:        app.Enabled,
			Pinned:         app.Pinned,
			JsonData:       app.JSONData,
			SecureJsonData: app.SecureJSONData,
			PluginVersion:  app.PluginVersion,
		}
		if err := bus.Dispatch(cmd); err != nil {
			return err
		}
	}

	return nil
}

func (ap *PluginProvisioner) applyChanges(configPath string) error {
	configs, err := ap.cfgProvider.readConfig(configPath)
	if err != nil {
		return err
	}

	for _, cfg := range configs {
		if err := ap.apply(cfg); err != nil {
			return err
		}
	}

	return nil
}
