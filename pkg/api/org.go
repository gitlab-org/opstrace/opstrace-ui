package api

import (
	"errors"

	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/api/dtos"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/api/response"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/bus"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/infra/metrics"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/models"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/setting"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/util"
)

// GET /api/group
func GetOrgCurrent(c *models.ReqContext) response.Response {
	return getOrgHelper(c.GroupId)
}

// GET /api/groups/:orgId
func GetOrgByID(c *models.ReqContext) response.Response {
	return getOrgHelper(c.ParamsInt64(":orgId"))
}

// Get /api/groups/name/:name
func (hs *HTTPServer) GetOrgByName(c *models.ReqContext) response.Response {
	org, err := hs.SQLStore.GetOrgByName(c.Params(":name"))
	if err != nil {
		if errors.Is(err, models.ErrGroupNotFound) {
			return response.Error(404, "Organization not found", err)
		}

		return response.Error(500, "Failed to get group", err)
	}
	result := models.GroupDetailsDTO{
		Id:   org.Id,
		Name: org.Name,
		Address: models.Address{
			Address1: org.Address1,
			Address2: org.Address2,
			City:     org.City,
			ZipCode:  org.ZipCode,
			State:    org.State,
			Country:  org.Country,
		},
	}

	return response.JSON(200, &result)
}

func getOrgHelper(orgID int64) response.Response {
	query := models.GetGroupByIdQuery{Id: orgID}

	if err := bus.Dispatch(&query); err != nil {
		if errors.Is(err, models.ErrGroupNotFound) {
			return response.Error(404, "Organization not found", err)
		}

		return response.Error(500, "Failed to get group", err)
	}

	org := query.Result
	result := models.GroupDetailsDTO{
		Id:   org.Id,
		Name: org.Name,
		Address: models.Address{
			Address1: org.Address1,
			Address2: org.Address2,
			City:     org.City,
			ZipCode:  org.ZipCode,
			State:    org.State,
			Country:  org.Country,
		},
	}

	return response.JSON(200, &result)
}

// POST /api/groups
func CreateOrg(c *models.ReqContext, cmd models.CreateGroupCommand) response.Response {
	if !c.IsSignedIn || (!setting.AllowUserOrgCreate && !c.IsGrafanaAdmin) {
		return response.Error(403, "Access denied", nil)
	}

	cmd.UserId = c.UserId
	if err := bus.Dispatch(&cmd); err != nil {
		if errors.Is(err, models.ErrGroupNameTaken) {
			return response.Error(409, "Organization name taken", err)
		}
		return response.Error(500, "Failed to create organization", err)
	}

	metrics.MApiOrgCreate.Inc()

	return response.JSON(200, &util.DynMap{
		"orgId":   cmd.Result.Id,
		"message": "Organization created",
	})
}

// PUT /api/group
func UpdateOrgCurrent(c *models.ReqContext, form dtos.UpdateOrgForm) response.Response {
	return updateOrgHelper(form, c.GroupId)
}

// PUT /api/groups/:orgId
func UpdateOrg(c *models.ReqContext, form dtos.UpdateOrgForm) response.Response {
	return updateOrgHelper(form, c.ParamsInt64(":orgId"))
}

func updateOrgHelper(form dtos.UpdateOrgForm, orgID int64) response.Response {
	cmd := models.UpdateGroupCommand{Name: form.Name, GroupId: orgID}
	if err := bus.Dispatch(&cmd); err != nil {
		if errors.Is(err, models.ErrGroupNameTaken) {
			return response.Error(400, "Group name taken", err)
		}
		return response.Error(500, "Failed to update group", err)
	}

	return response.Success("Group updated")
}

// PUT /api/group/address
func UpdateOrgAddressCurrent(c *models.ReqContext, form dtos.UpdateOrgAddressForm) response.Response {
	return updateOrgAddressHelper(form, c.GroupId)
}

// PUT /api/groups/:orgId/address
func UpdateOrgAddress(c *models.ReqContext, form dtos.UpdateOrgAddressForm) response.Response {
	return updateOrgAddressHelper(form, c.ParamsInt64(":orgId"))
}

func updateOrgAddressHelper(form dtos.UpdateOrgAddressForm, orgID int64) response.Response {
	cmd := models.UpdateGroupAddressCommand{
		GroupId: orgID,
		Address: models.Address{
			Address1: form.Address1,
			Address2: form.Address2,
			City:     form.City,
			State:    form.State,
			ZipCode:  form.ZipCode,
			Country:  form.Country,
		},
	}

	if err := bus.Dispatch(&cmd); err != nil {
		return response.Error(500, "Failed to update group address", err)
	}

	return response.Success("Address updated")
}

// GET /api/groups/:groupId
func DeleteOrgByID(c *models.ReqContext) response.Response {
	if err := bus.Dispatch(&models.DeleteGroupCommand{Id: c.ParamsInt64(":orgId")}); err != nil {
		if errors.Is(err, models.ErrGroupNotFound) {
			return response.Error(404, "Failed to delete group. ID not found", nil)
		}
		return response.Error(500, "Failed to update group", err)
	}
	return response.Success("Group deleted")
}

func SearchOrgs(c *models.ReqContext) response.Response {
	perPage := c.QueryInt("perpage")
	if perPage <= 0 {
		perPage = 1000
	}

	page := c.QueryInt("page")

	query := models.SearchGroupsQuery{
		Query: c.Query("query"),
		Name:  c.Query("name"),
		Page:  page,
		Limit: perPage,
	}

	if err := bus.Dispatch(&query); err != nil {
		return response.Error(500, "Failed to search groups", err)
	}

	return response.JSON(200, query.Result)
}
