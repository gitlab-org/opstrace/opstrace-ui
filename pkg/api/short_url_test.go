package api

import (
	"context"
	"encoding/json"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/api/dtos"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/api/response"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/models"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/services/shorturls"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/services/sqlstore"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/setting"
)

func TestShortURLAPIEndpoint(t *testing.T) {
	settings := setting.NewCfg()
	hs := &HTTPServer{
		log: &FakeLogger{},
		Cfg: settings,
		ShortURLService: &shorturls.ShortURLService{
			SQLStore: sqlstore.InitTestDB(t),
		},
	}

	loggedInUserScenario(t, "When creating a new short URL", "/short-urls", func(sc *scenarioContext) {
		testPath := ""
		sc.handlerFunc = func(c *models.ReqContext) response.Response {
			return hs.createShortURL(c, dtos.CreateShortURLCmd{
				Path: testPath,
			})
		}
		sc.m.Post("/short-urls", sc.defaultHandler)
		t.Run("it should 400 when using an absolute path", func(t *testing.T) {
			testPath = "/absolute/path"

			sc.fakeReqWithParams("POST", sc.url, nil).exec()
			require.Equal(t, http.StatusBadRequest, sc.resp.Code)
		})

		t.Run("it should 200 and return uid when using a relative path", func(t *testing.T) {
			testPath = "relative/path"

			sc.fakeReqWithParams("POST", sc.url, nil).exec()
			require.Equal(t, http.StatusOK, sc.resp.Code)

			resp := &dtos.ShortURL{}
			require.NoError(t, json.NewDecoder(sc.resp.Body).Decode(resp))
			assert.NotEmpty(t, resp.UID)
			assert.NotEmpty(t, resp.URL)
		})
	})
}

func TestShortURLRedirectAPIEndpoint(t *testing.T) {
	origAppURL := setting.AppUrl
	t.Cleanup(func() { setting.AppUrl = origAppURL })
	const appUrl = "http://grafana.local/"
	setting.AppUrl = appUrl

	settings := setting.NewCfg()
	hs := &HTTPServer{
		log: &FakeLogger{},
		Cfg: settings,
		ShortURLService: &shorturls.ShortURLService{
			SQLStore: sqlstore.InitTestDB(t),
		},
	}

	loggedInUserScenario(t, "When going to a short URL", "/goto/:uid", func(sc *scenarioContext) {
		const relativePath = "relative/path"
		url, err := hs.ShortURLService.CreateShortURL(context.Background(), &models.SignedInUser{
			UserId:  testUserID,
			GroupId: testGroupID,
		}, relativePath)
		require.NoError(t, err)
		sc.handlerFunc = hs.redirectFromShortURL

		t.Run("invalid uid should 400", func(t *testing.T) {
			sc.fakeReqWithParams("GET", "/goto/**", nil).exec()
			require.Equal(t, http.StatusBadRequest, sc.resp.Code)
		})

		t.Run("unknown uid should 404", func(t *testing.T) {
			sc.fakeReqWithParams("GET", "/goto/unknown", nil).exec()
			require.Equal(t, http.StatusNotFound, sc.resp.Code)
		})

		t.Run("should redirect to known uid", func(t *testing.T) {
			sc.fakeReqWithParams("GET", "/goto/"+url.Uid, nil).exec()
			require.Equal(t, http.StatusFound, sc.resp.Code)
			require.Equal(t, appUrl+relativePath, sc.resp.Header().Get("Location"))
		})

		t.Run("should redirect with query params added", func(t *testing.T) {
			sc.fakeReqWithParams("GET", "/goto/"+url.Uid+"?query=param", nil).exec()
			require.Equal(t, http.StatusFound, sc.resp.Code)
			require.Equal(t, appUrl+relativePath+"?query=param", sc.resp.Header().Get("Location"))
		})
	})
}
