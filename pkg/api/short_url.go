package api

import (
	"errors"
	"fmt"
	"net/url"
	"path"
	"strings"

	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/api/dtos"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/api/response"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/models"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/setting"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/util"
)

// createShortURL handles requests to create short URLs.
func (hs *HTTPServer) createShortURL(c *models.ReqContext, cmd dtos.CreateShortURLCmd) response.Response {
	hs.log.Debug("Received request to create short URL", "path", cmd.Path)

	cmd.Path = strings.TrimSpace(cmd.Path)

	if path.IsAbs(cmd.Path) {
		hs.log.Error("Invalid short URL path", "path", cmd.Path)
		return response.Error(400, "Path should be relative", nil)
	}

	shortURL, err := hs.ShortURLService.CreateShortURL(c.Req.Context(), c.SignedInUser, cmd.Path)
	if err != nil {
		return response.Error(500, "Failed to create short URL", err)
	}

	url := fmt.Sprintf("%s/goto/%s", strings.TrimSuffix(setting.AppUrl, "/"), shortURL.Uid)
	c.Logger.Debug("Created short URL", "url", url)

	dto := dtos.ShortURL{
		UID: shortURL.Uid,
		URL: url,
	}

	return response.JSON(200, dto)
}

func (hs *HTTPServer) redirectFromShortURL(c *models.ReqContext) response.Response {
	shortURLUID := c.Params(":uid")

	if !util.IsValidShortUID(shortURLUID) {
		return response.Error(400, "Invalid short URL UID", nil)
	}

	shortURL, err := hs.ShortURLService.GetShortURLByUID(c.Req.Context(), c.SignedInUser, shortURLUID)
	if err != nil {
		if errors.Is(err, models.ErrShortURLNotFound) {
			hs.log.Debug("Not redirecting short URL since not found")
			return response.Error(404, "Short URL not found", nil)
		}

		hs.log.Error("Short URL redirection error", "error", err)
		return response.Error(500, "Failed to redirect short URL", err)
	}

	// Failure to update LastSeenAt should still allow to redirect
	if err := hs.ShortURLService.UpdateLastSeenAt(c.Req.Context(), shortURL); err != nil {
		hs.log.Error("Failed to update short URL last seen at", "error", err)
	}

	originalUrl, err := url.Parse(shortURL.Path)
	if err != nil {
		hs.log.Error("Failed to parse short URL path", "error", err)
		return response.Error(500, "Failed to redirect short URL", err)
	}

	extraQuery := c.Req.URL.Query()
	addExtraQuery(originalUrl, extraQuery)

	absUrl := setting.ToAbsUrl(originalUrl.String())
	hs.log.Debug("Redirecting short URL", "url", absUrl)
	return response.Redirect(absUrl)
}

// Add extra query params from `extraQuery` to `originalUrl`
func addExtraQuery(originalUrl *url.URL, extraQuery url.Values) {
	values := originalUrl.Query()

	for key, vals := range extraQuery {
		for _, val := range vals {
			values.Add(key, val)
		}
	}

	originalUrl.RawQuery = values.Encode()
}
