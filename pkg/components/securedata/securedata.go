package securedata

import (
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/setting"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/util"
)

type SecureData []byte

func Encrypt(data []byte) (SecureData, error) {
	return util.Encrypt(data, setting.SecretKey)
}

func (s SecureData) Decrypt() ([]byte, error) {
	return util.Decrypt(s, setting.SecretKey)
}
